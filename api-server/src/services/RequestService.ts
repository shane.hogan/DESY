import { IRequestService } from "../interfaces/IRequestService";
import Request, { IRequestModel } from "../models/Request";

export class RequestService implements IRequestService {
  getAll(): Promise<IRequestModel[]> {
    return Request.find({}).exec();
  }

  getAllOpen(): Promise<IRequestModel[]> {
    return Request.find({ status: "OPEN" }).exec();
  }

  submitRequest(request: IRequestModel): Promise<IRequestModel> {
    return Request.create(request);
  }

  updateRequest(request: IRequestModel): Promise<IRequestModel> {
    return Request.updateOne({ _id: request.id }, request).exec();
  }

  getOne(id: String): Promise<IRequestModel> {
    return Request.findById(id).exec();
  }

  getAllWithFilter(options: any): Promise<IRequestModel[]> {
    return Request.find(options || {}).exec();
  }
  getOneWithFilter(options: any): Promise<IRequestModel> {
    return Request.findOne(options || {}).exec();
  }
  deleteOne(id: String): Promise<IRequestModel> {
    return Request.findOneAndRemove({ _id: id }).exec();
  }
  allExecutionTimes(): Promise<IRequestModel[]> {
    return Request.find({})
      .where("executionTime")
      .ne(null)
      .exec();
  }
  filteredExecutionTimes(dataSource: string): Promise<IRequestModel[]> {
    return Request.find({ dataSource: dataSource })
      .where("executionTime")
      .ne(null)
      .exec();
  }

  getAllDataSources(): Promise<string[]> {
    return Request.find()
      .distinct("dataSource")
      .exec();
  }
  count(conditions: any): Promise<number> {
    return Request.count(conditions || {}).exec();
  }
  findAllWithStatus(): Promise<IRequestModel[]> {
    return Request.find()
      .where("status")
      .ne(null)
      .exec();
  }
}
