import { IAwsService } from "../interfaces/IAwsService";
import * as AWS from "aws-sdk";
import { IRequestModel } from "../models/Request";

export class AwsService implements IAwsService {
  private glue: AWS.Glue;

  constructor() {
    AWS.config.update({
      accessKeyId: "AKIAJBNEPHNSEWHDK3DQ",
      secretAccessKey: "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",
      region: "us-east-1"
    });
    this.glue = new AWS.Glue();
  }

  getTables(): Promise<any> {
    return this.glue
      .getTables({
        DatabaseName: "cacmsdemo"
      })
      .promise();
  }

  startJob(jobParams: IRequestModel): Promise<any> {
    return this.glue
      .startJobRun({
        JobName: jobParams.dataSource,
        Arguments: {
          "--OutputFileName": jobParams.fileName,
          "--UserName": "cmsuser2",
          "--DataFormat": "JSON", //Ignore input, only do JSON for now
          "--DUANNumber": jobParams.DUANNumber,
          "--Date": "12-12-2082"
        }
      })
      .promise();
  }

  getJobs(): Promise<any> {
    return this.glue.getJobs().promise();
  }

  getRanJob(jobName: string, jobId: string): Promise<any> {
    return this.glue.getJobRun({ JobName: jobName, RunId: jobId }).promise();
  }
}
