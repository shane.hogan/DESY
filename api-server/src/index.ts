import app from "./app";
import * as fs from "fs";
import * as https from "https";
import { connect } from "mongoose";
const port = process.env.PORT || 8443;

const options = {
  key: fs.readFileSync("/home/ubuntu/SSLCerts/express-key.key.insecure"),
  cert: fs.readFileSync("/home/ubuntu/SSLCerts/express-server.crt")
};
https.createServer(options, app).listen(8443, () => {
  connect("mongodb://127.0.0.1:27017/desy")
    .then(() => console.log("Connected to Mongo"))
    .catch(e => console.log("Failed to connect to Mongo ", e));
  return console.log("Listening on Port : " + "8443");
});
