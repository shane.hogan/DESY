import { IRequest } from "../interfaces/IRequest";
import { Document, model, Schema } from "mongoose";

export interface IRequestModel extends IRequest, Document {}

export const RequestSchema = new Schema(
  {
    userId: { type: String, required: true },
    dataSource: { type: String, required: true },
    fileName: { type: String, required: true },
    format: String,
    dadosNumber: String,
    status: String,
    jobId: String,
    DUANNumber: { type: String, required: true },
    executionTime: Number,
    startedOn: Date,
    completedOn: Date,
    allocatedCapacity: Number
  },
  {
    timestamps: {}
  }
);

RequestSchema.pre<IRequestModel>("save", function(next) {
  let request = this;
  if (!request.status || request.status.trim() === "") {
    request.status = "STARTING";
  }
  next();
});

const Request = model<IRequestModel>("Request", RequestSchema);

export default Request;
