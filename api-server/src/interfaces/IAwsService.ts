export interface IAwsService {
  getTables(): Promise<any>;
  startJob(jobParams): Promise<any>;
  getJobs(): Promise<any>;
  getRanJob(jobName: string, jobId: string): Promise<any>;
}
