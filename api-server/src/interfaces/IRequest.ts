export interface IRequest {
  dataSource: string;
  fileName: string;
  format: string;
  userId: string;
  dadosNumber: string;
  status: string;
  jobId: string;
  DUANNumber: string;
  executionTime: number;
  startedOn: Date;
  completedOn: Date;
  allocatedCapacity: number;
  updatedAt: Date;
  createdAt: Date;
}
