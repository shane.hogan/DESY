import { IRequestModel } from "../models/Request";
import { promises } from "fs";

export interface IRequestService {
  getOne(id: String): Promise<IRequestModel>;
  getAll(): Promise<IRequestModel[]>;
  getAllOpen(): Promise<IRequestModel[]>;
  getAllWithFilter(options: any): Promise<IRequestModel[]>;
  submitRequest(request: IRequestModel): Promise<IRequestModel>;
  updateRequest(
    request: IRequestModel | IRequestModel[]
  ): Promise<IRequestModel>;
  getOneWithFilter(options: any): Promise<IRequestModel>;
  deleteOne(id: String): Promise<IRequestModel>;
  allExecutionTimes(): Promise<IRequestModel[]>;
  filteredExecutionTimes(datasource: string): Promise<IRequestModel[]>;
  getAllDataSources(): Promise<string[]>;
  count(conditions: any): Promise<number>;
  findAllWithStatus(): Promise<IRequestModel[]>;
}
