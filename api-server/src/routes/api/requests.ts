import { Request, Router, Response } from "express";
import { IRequestService } from "../../interfaces/IRequestService";
import { RequestService } from "../../services/RequestService";
import { Types } from "mongoose";
import * as AWS from "aws-sdk";
import { IAwsService } from "../../interfaces/IAwsService";
import { AwsService } from "../../services/AwsService";
import * as _ from "lodash";
import RequestModel, { IRequestModel } from "../../models/Request";

namespace Route {
  export class RequestsApi {
    router: Router;
    requestService: IRequestService;
    awsService: IAwsService;

    constructor() {
      this.router = Router();
      this.requestService = new RequestService();
      this.awsService = new AwsService();

      this.router.get("/all", this.getAll.bind(this));
      this.router.get("/all/open", this.getAllOpen.bind(this));
      this.router.post("/update", this.updateRanJobs.bind(this));
      this.router.post("/updateOne", this.updateRanJob.bind(this));
      this.router.post("/updateAndWait", this.updateJobsAndWait.bind(this));
      this.router.get("/jobDetails/:jobId", this.getJobDetails.bind(this));
      this.router.get(
        "/:id/all/:status",
        this.getAllWithUserIdAndStatus.bind(this)
      );
      this.router.get("/:id/all", this.getAllWithUserId.bind(this));
      this.router.get("/:id", this.getOne.bind(this));
      this.router.post("/", this.createRequest.bind(this));
    }

    private getAllWithUserIdAndStatus(req: Request, res: Response) {
      const userId = req.params.id;
      const status = req.params.status;
      this.requestService
        .getAllWithFilter({ userId: userId, status: status })
        .then(requests => res.json(requests))
        .catch(err => res.status(500).json(err));
    }

    private getAllWithUserId(req: Request, res: Response) {
      const userId = req.params.id;
      this.requestService
        .getAllWithFilter({ userId: userId })
        .then(requests => res.json(requests))
        .catch(err => res.status(500).json(err));
    }

    private getOne(req: Request, res: Response) {
      const id = req.params.id;
      if (!Types.ObjectId.isValid(id)) {
        return res.status(500).json({ error: "Invalid Id" });
      }
      this.requestService
        .getOne(req.params.id)
        .then(request => {
          res.json(request);
        })
        .catch(err => {
          res.status(500).json(err);
        });
    }

    private getAll(req: Request, res: Response) {
      this.requestService
        .getAll()
        .then(requests => {
          res.json(requests);
        })
        .catch(err => {
          res.status(500).json(err);
        });
    }

    private getAllOpen(req: Request, res: Response) {
      this.requestService
        .getAllOpen()
        .then(requests => {
          res.json(requests);
        })
        .catch(err => {
          res.status(500).json(err);
        });
    }

    private async createRequest(req: Request, res: Response) {
      let newRequest;
      try {
        newRequest = await this.requestService.submitRequest(req.body);
        const awsJobResponse = await this.awsService.startJob(newRequest);
        newRequest.jobId = awsJobResponse.JobRunId;
        const updatedRequest = await this.requestService.updateRequest(
          newRequest
        );
        res.json(updatedRequest);
      } catch (e) {
        await this.requestService.deleteOne(newRequest.id);
        res.status(500).json(e);
      }
    }

    updateRanJobs(req: Request, res: Response) {
      const { userId } = req.body;
      this.requestService
        .getAllWithFilter({ userId: userId })
        .then(jobs => {
          res.json(jobs);
          _.forEach(jobs, job => {
            this.awsService
              .getRanJob(job.dataSource, job.jobId)
              .then(ranJob => {
                job.status = ranJob.JobRun.JobRunState;
                job.completedOn = ranJob.JobRun.CompletedOn;
                job.startedOn = ranJob.JobRun.StartedOn;
                job.executionTime = ranJob.JobRun.ExecutionTime;
                job.allocatedCapacity = ranJob.JobRun.AllocatedCapacity;
                this.requestService.updateRequest(job);
              })
              .catch(err => {});
          });
        })
        .catch(err => res.status(500).json(err));
    }

    async updateJobsAndWait(req: Request, res: Response) {
      const { userId } = req.body;
      try {
        let requests = await this.requestService.getAllWithFilter({
          userId: userId
        });
        await Promise.all(
          requests.map(async job => {
            let ranJob = await this.awsService.getRanJob(
              job.dataSource,
              job.jobId
            );
            job.status = ranJob.JobRun.JobRunState;
            job.completedOn = ranJob.JobRun.CompletedOn;
            job.startedOn = ranJob.JobRun.StartedOn;
            job.executionTime = ranJob.JobRun.ExecutionTime;
            job.allocatedCapacity = ranJob.JobRun.AllocatedCapacity;
            await this.requestService.updateRequest(job);
          })
        );
        requests = await this.requestService.getAllWithFilter({
          userId: userId
        });
        res.json(requests);
      } catch (e) {
        res.status(500).json(e);
      }
    }

    updateRanJob(req: Request, res: Response) {
      const { jobId } = req.body;
      this.requestService
        .getOneWithFilter({ jobId: jobId })
        .then(job => {
          this.awsService
            .getRanJob(job.dataSource, job.jobId)
            .then(ranJob => {
              job.status = ranJob.JobRun.JobRunState;
              job.completedOn = ranJob.JobRun.CompletedOn;
              job.startedOn = ranJob.JobRun.StartedOn;
              job.executionTime = ranJob.JobRun.ExecutionTime;
              job.allocatedCapacity = ranJob.JobRun.AllocatedCapacity;
              this.requestService
                .updateRequest(job)
                .then(job => res.json(job))
                .catch(err => res.status(500).json(err));
            })
            .catch(err => res.status(500).json(err));
        })
        .catch(err => res.status(500).json(err));
    }
    getJobDetails(req: Request, res: Response) {
      const { jobId } = req.params;

      this.requestService
        .getOneWithFilter({ jobId: jobId })
        .then(job => {
          this.awsService
            .getRanJob(job.dataSource, job.jobId)
            .then(ranJob => {
              res.json(ranJob.JobRun);
            })
            .catch(err => res.status(500).json(err));
        })
        .catch(err => res.status(500).json(err));
    }
  }
}

const requests = new Route.RequestsApi();
export default requests;
