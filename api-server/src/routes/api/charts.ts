import { IRequestService } from "../../interfaces/IRequestService";
import { IAwsService } from "../../interfaces/IAwsService";
import { NextFunction, Request, Response, Router } from "express";
import { AwsService } from "../../services/AwsService";
import { RequestService } from "../../services/RequestService";
import * as _ from "lodash";
import requests from "./requests";
import { raw } from "body-parser";
import * as moment from "moment";

namespace Route {
  export class ChartsApi {
    router: Router;
    requestsService: IRequestService;

    constructor() {
      this.router = Router();
      this.requestsService = new RequestService();

      this.router.get("/timeSeries/all", this.timeSeriesChart.bind(this));
      this.router.get("/pie/all", this.countByStatusChart.bind(this));
      this.router.get(
        "/gauge/averageExecution",
        this.averageExecutionTime.bind(this)
      );
      this.router.get("/all", this.getAll.bind(this));
    }

    private async getAll(req: Request, res: Response) {
      let allRequests = await this.requestsService.getAll();
      let alllChartData = [
        await this.averageExecutionTimeFormatted(allRequests),
        await this.completedPerDayFormatted(allRequests),
        await this.totalByStatusFormatted(allRequests)
      ];
      res.json(alllChartData);
    }

    private async timeSeriesChart(req: Request, res: Response) {
      let allRequests = await this.requestsService.getAll();
      res.json(await this.completedPerDayRawData(allRequests));
    }

    private async averageExecutionTime(req: Request, res: Response) {
      let allRequests = await this.requestsService.getAll();

      res.json(await this.averageExecutionTimeRawData(allRequests));
    }

    private async countByStatusChart(req: Request, res: Response) {
      let allRequests = await this.requestsService.getAll();
      res.json(await this.totalByStatusRawData(allRequests));
    }

    private async completedPerDayRawData(allRequests) {
      allRequests = _.filter(allRequests, function(o) {
        return !!o.completedOn;
      });
      const grouped = _.countBy(allRequests, req => {
        return moment(req.completedOn).format("YYYY-MM-DD");
      });
      return grouped;
    }

    private async averageExecutionTimeRawData(allRequests) {
      allRequests = _.filter(allRequests, function(o) {
        return !!o.executionTime;
      });
      return _.meanBy(allRequests, "executionTime");
    }

    private async totalByStatusRawData(allRequests) {
      allRequests = _.filter(allRequests, function(o) {
        return !!o.status;
      });
      return _.countBy(allRequests, "status");
    }

    private async totalByStatusFormatted(allRequests) {
      const rawData = await this.totalByStatusRawData(allRequests);
      let columns = [];
      for (let key in rawData) {
        columns.push([key, rawData[key]]);
      }
      return {
        chartName: "Status Totals",
        helperText: `The chart above illustrates the proportions of ETL requests for the given statuses. <br/> 
        <i>Note: If the status is not in the chart, then there are currently no ETL Requests with that current status.</i>`,
        chart: {
          data: {
            columns: columns,
            type: "pie"
          }
        }
      };
    }

    private async averageExecutionTimeFormatted(allRequests) {
      const rawData = await this.averageExecutionTimeRawData(allRequests);
      return {
        chartName: "Average Execution Time",
        helperText: `<p>This chart displays the current average completion time for one ETL Job. Currently the average amount of time needed to complete a single ETL process is ${_.round(
          rawData,
          2
        )} seconds. This average is calculated using <u>${
          allRequests.length
        }</u> requests.</p>`,
        chart: {
          data: {
            columns: [["Average Execution Time", rawData]],
            type: "gauge"
          },
          gauge: {
            min: 0,
            max: rawData < 60 ? 60 : Math.floor(rawData + 50),
            units: " sec."
          }
        }
      };
    }

    private async completedPerDayFormatted(allRequests) {
      const rawData = (await this.completedPerDayRawData(allRequests)) as any;
      let xColumn = ["x"];
      let dataPoints = ["Total"];
      let values = [];
      let max = 0;
      for (let key in rawData) {
        xColumn.push(key);
        let val = rawData[key];
        if (val > max) max = val;
        dataPoints.push(val);
      }
      for (let i = 0; i < max + 5; i++) values.push(i);
      return {
        chartName: "ETL Requests Completed Per Day",
        helperText: `The graph above shows the total number of requests made on a given day. <br/>
        <i>Note: The date format is YYYY-MM-DD. Hovering on a data point will show the value in a larger pane.</i>`,
        chart: {
          data: {
            x: "x",
            columns: [xColumn, dataPoints]
          },
          zoom: {
            enabled: false
          },
          axis: {
            x: {
              type: "timeseries",
              tick: {
                format: "%Y-%m-%d"
              }
            },
            y: {
              tick: {
                values: values
              }
            }
          }
        }
      };
    }
  }
}

const charts = new Route.ChartsApi();
export default charts;
