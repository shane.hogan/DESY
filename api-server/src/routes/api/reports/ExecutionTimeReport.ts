import { IRequestService } from "../../../interfaces/IRequestService";
import { Response, Router, Request } from "express";
import { RequestService } from "../../../services/RequestService";
import * as _ from "lodash";

namespace Route {
  export class ExecutionTimeReport {
    router: Router;
    requestsService: IRequestService;

    constructor() {
      this.router = Router();
      this.requestsService = new RequestService();
      this.router.get("/", this.allAverage.bind(this));
      this.router.get("/allAverages", this.allAverages.bind(this));
      this.router.get("/:dataSource", this.speceficAverage.bind(this));
    }

    /**
     * Average execution time for all requests
     * @param {e.Request} req
     * @param {e.Response} res
     */
    private async allAverage(req: Request, res: Response) {
      const result = await this.requestsService.allExecutionTimes();
      res.json(this.meanAndRound(result));
    }

    /**
     * Average Execution time for a specific data source
     * @param {e.Request} req
     * @param {e.Response} res
     */
    private async speceficAverage(req: Request, res: Response) {
      const result = await this.requestsService.filteredExecutionTimes(
        req.params.dataSource
      );
      res.json(this.meanAndRound(result));
    }

    /**
     * An array of averages of each type
     * @param {Request} req
     * @param {e.Response} res
     * @returns {Promise<void>}
     */
    private async allAverages(req: Request, res: Response) {
      let averages = [];
      const allAvg = await this.requestsService.allExecutionTimes();
      averages.push({
        dataSource: "All",
        average: this.meanAndRound(allAvg)
      });
      const dataSources = await this.requestsService.getAllDataSources();
      await Promise.all(
        dataSources.map(async source => {
          const result = await this.requestsService.filteredExecutionTimes(
            source
          );
          averages.push({
            dataSource: source,
            average: this.meanAndRound(result)
          });
        })
      );
      res.json(averages);
    }

    private meanAndRound(result) {
      return _.round(_.meanBy(result, "executionTime") || 0, 2);
    }
  }
}
const executionTime = new Route.ExecutionTimeReport();
export default executionTime;
