import { IRequestService } from "../../../interfaces/IRequestService";
import { NextFunction, Router, Request, Response } from "express";
import { RequestService } from "../../../services/RequestService";
import * as moment from "moment";
import * as _ from "lodash";

namespace Route {
  export class CountsReports {
    router: Router;
    requestsService: IRequestService;

    constructor() {
      this.router = Router();
      this.requestsService = new RequestService();

      this.router.get("/dataSourceTotals", this.dataSourceTotals.bind(this));
      this.router.get("/dailyTotals", this.dailyTotals.bind(this));
      this.router.get("/statusTotals", this.statusTotals.bind(this));
    }

    private async statusTotals(req: Request, res: Response) {
      let requestsWithStatus = await this.requestsService.findAllWithStatus();
      let counted = _.countBy(requestsWithStatus, "status");
      let columns = [];
      for (let key in counted) {
        if (counted.hasOwnProperty(key)) {
          columns.push([key, counted[key]]);
        }
      }
      res.json({
        data: columns
      });
    }

    /**
     * An array of total numbers of request of each data source
     * @param {e.Request} req
     * @param {e.Response} res
     * @param {e.NextFunction} next
     * @returns {Promise<void>}
     */
    private async dataSourceTotals(
      req: Request,
      res: Response,
      next: NextFunction
    ) {
      let totals = [];
      try {
        const dataSources = await this.requestsService.getAllDataSources();
        await Promise.all(
          dataSources.map(async source => {
            const result = await this.requestsService.count({
              dataSource: source
            });
            totals.push([source.replace("Job", ""), result]);
          })
        );

        res.json({
          data: totals
        });
      } catch (e) {
        next(e);
      }
    }

    /**
     * Daily Totals
     * @param {e.Request} req
     * @param {e.Response} res
     * @param {e.NextFunction} next
     * @returns {Promise<void>}
     */
    private async dailyTotals(req: Request, res: Response, next: NextFunction) {
      let dates = ["x"];
      let dataPoints = [];
      let allRequests = await this.requestsService.getAll();
      let max = 0;
      allRequests.forEach(request => {
        let formattedDate = moment(request.updatedAt).format("YYYY-MM-DD");
        if (dates.indexOf(formattedDate) === -1) dates.push(formattedDate);
        let currIndex = dates.indexOf(formattedDate);
        let statusArr = dataPoints.find(
          arr =>
            arr[0].toLowerCase().trim() === request.status.toLowerCase().trim()
        );
        if (!statusArr) {
          statusArr = [request.status];
          dataPoints.push(statusArr);
        }
        if (!statusArr[currIndex]) statusArr[currIndex] = 1;
        else statusArr[currIndex]++;

        if (statusArr[currIndex] > max) max = statusArr[currIndex];
      });

      for (let statusArrs in dataPoints) {
        let currArr = dataPoints[statusArrs];
        for (let i = 1; i < dates.length; i++) {
          if (!currArr[i] || currArr[i] === null) {
            currArr[i] = 0;
          }
        }
      }

      let yTicks = Array.from({ length: max }, (v, i) => i);

      res.json({
        dates: dates,
        data: dataPoints,
        yTicks: yTicks
      });
    }
  }
}
const counts = new Route.CountsReports();
export default counts;
