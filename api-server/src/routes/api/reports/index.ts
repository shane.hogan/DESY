import { Router, Response, Request, NextFunction } from "express";
import ExecutionTimeReport from "./ExecutionTimeReport";
import CountsReport from "./CountsReports";

namespace Route {
  export class Reports {
    public router: Router;

    constructor() {
      this.router = Router();
      this.router.use(
        "/executionTime",
        ExecutionTimeReport.router.bind(ExecutionTimeReport.router)
      );
      this.router.use("/count", CountsReport.router.bind(CountsReport.router));
    }
  }
}

const ReportsRouter = new Route.Reports();
export default ReportsRouter;
