import { Response, Router } from "express";
import Request from "../../models/Request";
import { IAwsService } from "../../interfaces/IAwsService";
import { AwsService } from "../../services/AwsService";
import * as _ from "lodash";

namespace Route {
  export class TablesApi {
    router: Router;
    awsService: IAwsService;

    constructor() {
      this.router = Router();
      this.awsService = new AwsService();
      this.router.get("/", this.getTables.bind(this));
    }

    private getTables(req: Request, res: Response) {
      this.awsService
        .getTables()
        .then(response => {
          res.json(this.parseTables(response));
        })
        .catch(err => {
          res.status(500).json(err);
        });
    }

    private parseTables(awsTables): any[] {
      return _.map(awsTables.TableList, table => {
        return {
          name: table.Name,
          description: table.Description,
          columns: table.StorageDescriptor.Columns
        };
      });
    }
  }
}

const tables = new Route.TablesApi();
export default tables;
