import { IRequestService } from "../../interfaces/IRequestService";
import { IAwsService } from "../../interfaces/IAwsService";
import { Request, Response, Router } from "express";
import { AwsService } from "../../services/AwsService";
import { RequestService } from "../../services/RequestService";
import * as _ from "lodash";

namespace Route {
  export class JobsApi {
    router: Router;
    awsService: IAwsService;

    constructor() {
      this.router = Router();
      this.awsService = new AwsService();

      this.router.get("/runnable", this.getJobs.bind(this));
    }
    private getJobs(req: Request, res: Response) {
      this.awsService
        .getJobs()
        .then(jobs => res.json({ jobs: _.map(jobs.Jobs, "Name") }))
        .catch(err => res.status(500).json(err));
    }
  }
}

const jobs = new Route.JobsApi();
export default jobs;
