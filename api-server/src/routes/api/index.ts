import { Router, Response, Request, NextFunction } from "express";
import { ExpressOIDC } from "@okta/oidc-middleware";
import * as OktaJwtVerifier from "@okta/jwt-verifier";
import RequestApi from "./requests";
import TablesApi from "./tables";
import ChartsApi from "./charts";
import ReportsApi from "./reports";
namespace ApiRoute {
  export class Api {
    public router: Router;
    private oktaJwtVerifier;

    constructor() {
      this.router = Router();
      this.oktaJwtVerifier = new OktaJwtVerifier({
        issuer: "https://dev-684300.oktapreview.com/oauth2",
        assertClaims: {
          aud: "0oafjysu682Op4ri70h7",
          cid: undefined
        }
      });

      //this.addAuth();
      this.router.get("/version", (req: Request, res: Response) => {
        res.json({
          version: "1.0.0"
        });
      });
      this.router.use("/requests", RequestApi.router.bind(RequestApi.router));
      this.router.use("/tables", TablesApi.router.bind(TablesApi.router));
      this.router.use("/charts", ChartsApi.router.bind(ChartsApi.router));
      this.router.use("/reports", ReportsApi.router.bind(ReportsApi.router));
    }

    addAuth() {
      this.router.use(
        "/",
        this.authenticationRequired.bind(this),
        (req, res, next) => {
          next();
        }
      );
    }

    authenticationRequired(req, res, next) {
      const authHeader = req.headers.authorization || "";
      const match = authHeader.match(/Bearer (.+)/);

      if (!match) {
        res.status(401);
        return next("Unauthorized");
      }

      const accessToken = match[1];

      return this.oktaJwtVerifier
        .verifyAccessToken(accessToken)
        .then(jwt => {
          req.jwt = jwt;
          next();
        })
        .catch(err => {
          res.status(401).send(err.message);
        });
    }
  }
}

export = ApiRoute;
