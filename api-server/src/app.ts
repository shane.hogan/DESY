import * as express from "express";
import * as path from "path";
import * as cookieParser from "cookie-parser";
import * as logger from "morgan";
import * as session from "express-session";
import * as ApiRouter from "./routes/api/index";
import * as https from "https";
import * as methodOverride from "method-override";
import { NextFunction, Request, Response } from "express";
class Server {
  app: express.Application;

  public static boostrap(): Server {
    return new Server();
  }

  constructor() {
    this.app = express();

    this.config();
    this.routes();
    this.app.use(this.errorHanlder.bind(this));
  }

  config() {
    this.app.use(logger("dev"));
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));
    this.app.use(cookieParser());
    this.app.use(methodOverride());
    this.app.use(
      session({
        secret: "secret",
        resave: true,
        saveUnitialized: false
      })
    );
  }

  routes() {
    let router: express.Router = express.Router();
    router.use(express.static(path.join(__dirname, "../../webapp/build")));

    const api: ApiRouter.Api = new ApiRouter.Api();
    router.use("/api", api.router.bind(api.router));

    //Catch all for react - client side routing
    router.get("*", function(req, res) {
      res.sendFile(path.resolve(__dirname, "../../webapp/build/index.html"));
    });
    this.app.use(router);
  }

  private errorHanlder(
    err: any,
    res: Response,
    req: Request,
    next: NextFunction
  ) {
    console.error(err);
    res.status(500).json();
  }
}

const server = Server.boostrap();
export default server.app;
