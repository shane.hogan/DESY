import React from "react";
import { renderInput } from "../components/controls";
import { Field, reduxForm } from "redux-form";
import { Col, Row } from "reactstrap";
import { Button } from "mdbreact";
import { required } from "./formUtils";

const LoginForm = props => {
  const { handleSubmit, invalid, submitting } = props;

  return (
    <form onSubmit={handleSubmit}>
      <Row>
        <Field
          component={renderInput}
          plceholder={"Username"}
          icon={"user"}
          name={"username"}
          label={"Username"}
          validate={[required]}
        />
      </Row>
      <Row>
        <Col className={"center"}>
          <div className="text-center mb-3">
            <Button
              type="submit"
              gradient="blue"
              rounded
              className="btn-block z-depth-1a"
              disabled={invalid || submitting}
            >
              Sign in
            </Button>
          </div>
        </Col>
      </Row>
    </form>
  );
};

export default reduxForm({
  form: "loginForm_UsernameOnly",
  initialValues: {
    username: "cmsuser1"
  }
})(LoginForm);
