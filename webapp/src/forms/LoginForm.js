import React from "react";
import { renderInput } from "../components/controls";
import { Field, reduxForm } from "redux-form";

const LoginForm = props => {
  const { handleSubmit, pristine, reset, submitting } = props;

  return (
    <form onSubmit={handleSubmit}>
      <Field component={renderInput} name={"username"} label={"Username"} />
      <Field
        component={renderInput}
        name={"password"}
        type={"password"}
        label={"Password"}
      />
      <button type="submit" disabled={pristine || submitting}>
        Submit
      </button>
      <button onClick={() => reset()}>Clear</button>
    </form>
  );
};

export default reduxForm({
  form: "loginForm"
})(LoginForm);
