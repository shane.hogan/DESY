import React from "react";
import { renderInput, renderSelect } from "../components/controls";
import { Field, reduxForm } from "redux-form";
import { Col, Row } from "reactstrap";
import Properties from "../properties";
import { Button } from "mdbreact";
import { required } from "./formUtils";
let { availableCatalogs, availableFormats } = Properties;
availableCatalogs.unshift({
  label: "Select Data Source",
  value: "",
  disabled: true
});
availableFormats.unshift({
  label: "Select ETL Format",
  value: "",
  disabled: true
});

const CreateRequestsForm = props => {
  const { handleSubmit, pristine, reset, submitting } = props;

  return (
    <form onSubmit={handleSubmit}>
      <Row>
        <Field
          component={renderSelect}
          validate={[required]}
          name={"dataSource"}
          label={"Data Source"}
          data={availableCatalogs}
        />
        <Field
          component={renderInput}
          name={"fileName"}
          label={"File Name"}
          validate={[required]}
        />
        <Field
          component={renderSelect}
          validate={[required]}
          name={"format"}
          label={"ETL Data Format"}
          data={availableFormats}
        />
        <Field
          component={renderInput}
          name={"DUANNumber"}
          label={"DUA Number"}
          validate={[required]}
        />
      </Row>
      <Row>
        <Col className={"center"}>
          <div className="text-center mb-3">
            <Button
              type="submit"
              gradient="blue"
              rounded
              className="btn-block z-depth-1a"
              disabled={pristine || submitting}
            >
              Submit Request
            </Button>
          </div>
          <div className="text-center mb-3">
            <Button
              outline
              color={"danger"}
              type="button"
              rounded
              className="btn-block z-depth-1a"
              onClick={reset.bind(this)}
            >
              Clear
            </Button>
          </div>
        </Col>
      </Row>
    </form>
  );
};

export default reduxForm({
  form: "createRequestsForm"
})(CreateRequestsForm);
