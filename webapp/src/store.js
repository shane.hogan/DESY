import { createStore, applyMiddleware, compose } from "redux";
import createHistory from "history/createBrowserHistory";
import reducers from "./reducers";
import thunk from "redux-thunk";
import storage from "redux-persist/lib/storage";
import { routerMiddleware } from "react-router-redux";
import { persistStore, persistReducer } from "redux-persist";
import hardSet from "redux-persist/lib/stateReconciler/hardSet";
const history = createHistory();

const middleware = routerMiddleware(history);

const persistedReducer = persistReducer(
  { key: "root", storage, stateReconciler: hardSet },
  reducers
);

const store = createStore(
  persistedReducer,
  compose(
    applyMiddleware(thunk, middleware),
    process.env.NODE_ENV !== "production" && window.devToolsExtension
      ? window.devToolsExtension()
      : f => f
  )
);
export const persistor = persistStore(store);
export default store;
