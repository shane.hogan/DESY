import React, { Component } from "react";
import "./App.css";
import NavHeader from "./containers/NavHeaderContainer";
import { Security, SecureRoute } from "@okta/okta-react";
import { Provider } from "react-redux";
import { ConnectedRouter } from "react-router-redux";
import { Route, Switch } from "react-router";
import store, { persistor } from "./store";
import createHistory from "history/createBrowserHistory";
import AboutPage from "./pages/AboutPage";
import LoginPage from "./containers/LoginContainer";
import { PersistGate } from "redux-persist/integration/react";
import RequestsStatusContainer from "./containers/RequestsStatusContainer";
import CreateRequestsContainer from "./containers/CreateRequestsContainer";
import TablesContainer from "./containers/TablesContainer";
import Footer from "./components/Footer";
import UserInfo from "./containers/UserInfoContainer";
import AuthWrapper from "./components/AuthWrapper";
import ReportsPage from "./pages/ReportsPage";
import ImplicitCallback from "./containers/ImplicitCallbackContainer";

const history = createHistory();

const issuer = process.env.REACT_APP_OKTA_ISSUER;
const clientId = process.env.REACT_APP_OKTA_CLIENT_ID;

function onAuthRequired({ history }) {
  history.push("/login");
}

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <ConnectedRouter history={history}>
            <Security
              issuer={issuer}
              client_id={clientId}
              redirect_uri={window.location.origin + "/implicit/callback"}
              onAuthRequired={onAuthRequired}
            >
              <AuthWrapper>
                <NavHeader />
                <UserInfo />
                <div className={"content"}>
                  <Switch>
                    <Route path="/" exact component={AboutPage} />
                    <Route path="/login" exact component={LoginPage} />
                    <Route path="/about" exact component={AboutPage} />
                    <SecureRoute
                      path="/requestStatus"
                      exact
                      component={RequestsStatusContainer}
                    />
                    <SecureRoute path="/catalogs" exact component={TablesContainer} />
                    <SecureRoute path="/reports" exact component={ReportsPage} />
                    <SecureRoute
                      path="/createRequest"
                      exact
                      component={CreateRequestsContainer}
                    />
                    <Route path="/implicit/callback" component={ImplicitCallback} />
                  </Switch>
                </div>
                <Footer />
              </AuthWrapper>
            </Security>
          </ConnectedRouter>
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
