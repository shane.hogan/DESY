import { api, dispatchToReducer } from "./actionUtils";
export const GET_ALL_CHARTS = "GET_ALL_CHARTS";
export const GET_ALL_CHARTS_SUCCESS = "GET_ALL_CHARTS_SUCCESS";
export const GET_ALL_CHARTS_FAILURE = "GET_ALL_CHARTS_FAILURE";
export const GET_EXEC_AVERAGES = "GET_EXEC_AVERAGES";
export const GET_EXEC_AVERAGES_SUCCESS = "GET_EXEC_AVERAGES_SUCCESS";
export const GET_EXEC_AVERAGES_FAILURE = "GET_EXEC_AVERAGES_FAILURE";
export const GET_DAILY_TOTALS = "GET_DAILY_TOTALS";
export const GET_DAILY_TOTALS_SUCCESS = "GET_DAILY_TOTALS_SUCCESS";
export const GET_DAILY_TOTALS_FAILURE = "GET_DAILY_TOTALS_FAILURE";
export const GET_STATUS_TOTALS = "GET_STATUS_TOTALS";
export const GET_STATUS_TOTALS_SUCCESS = "GET_STATUS_TOTALS_SUCCESS";
export const GET_STATUS_TOTALS_FAILURE = "GET_STATUS_TOTALS_FAILURE";
export const GET_DATA_SOURCE_TOTALS = "GET_DATA_SOURCE_TOTALS";
export const GET_DATA_SOURCE_TOTALS_SUCCESS = "GET_DATA_SOURCE_TOTALS_SUCCESS";
export const GET_DATA_SOURCE_TOTALS_FAILURE = "GET_DATA_SOURCE_TOTALS_FAILURE";

export const getAllCharts = () => {
  return dispatch => {
    dispatch(getDailyTotals());
    dispatch(getExecutionAverages());
    dispatch(getdataSourceTotals());
    dispatch(getStatusTotals());
  };
};

export const getExecutionAverages = () => {
  return dispatch => {
    dispatch(dispatchToReducer(GET_EXEC_AVERAGES));
    api()
      .get("/reports/executionTime/allAverages")
      .then(res => {
        const data = res.data;
        if (res.ok) {
          dispatch(dispatchToReducer(GET_EXEC_AVERAGES_SUCCESS, data));
        } else {
          dispatch(dispatchToReducer(GET_EXEC_AVERAGES_FAILURE, data));
        }
      });
  };
};

export const getDailyTotals = () => {
  return dispatch => {
    dispatch(dispatchToReducer(GET_DAILY_TOTALS));
    api()
      .get("/reports/count/dailyTotals")
      .then(res => {
        const data = res.data;
        if (res.ok) {
          dispatch(dispatchToReducer(GET_DAILY_TOTALS_SUCCESS, data));
        } else {
          dispatch(dispatchToReducer(GET_DAILY_TOTALS_FAILURE, data));
        }
      });
  };
};

export const getdataSourceTotals = () => {
  return dispatch => {
    dispatch(dispatchToReducer(GET_DATA_SOURCE_TOTALS));
    api()
      .get("/reports/count/dataSourceTotals")
      .then(res => {
        const data = res.data;
        if (res.ok) {
          dispatch(dispatchToReducer(GET_DATA_SOURCE_TOTALS_SUCCESS, data));
        } else {
          dispatch(dispatchToReducer(GET_DATA_SOURCE_TOTALS_FAILURE, data));
        }
      });
  };
};

export const getStatusTotals = () => {
  return dispatch => {
    dispatch(dispatchToReducer(GET_STATUS_TOTALS));
    api()
      .get("/reports/count/statusTotals")
      .then(res => {
        const data = res.data;
        if (res.ok) {
          dispatch(dispatchToReducer(GET_STATUS_TOTALS_SUCCESS, data));
        } else {
          dispatch(dispatchToReducer(GET_STATUS_TOTALS_FAILURE, data));
        }
      });
  };
};
