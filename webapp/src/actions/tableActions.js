import { api, dispatchToReducer } from "./actionUtils";
export const RETRIEVE_TABLES = "RETRIEVE_TABLES";
export const RETRIEVE_TABLES_SUCCESS = "RETRIEVE_TABLES_SUCCESS";
export const RETRIEVE_TABLES_FAILURE = "RETRIEVE_TABLES_FAILURE";

export const getTables = () => {
  return dispatch => {
    dispatch(dispatchToReducer(RETRIEVE_TABLES));
    api()
      .get("/tables")
      .then(res => {
        const data = res.data;
        if (!res.ok) {
          dispatch(dispatchToReducer(RETRIEVE_TABLES_FAILURE, data));
        } else {
          dispatch(dispatchToReducer(RETRIEVE_TABLES_SUCCESS, data));
        }
      });
  };
};
