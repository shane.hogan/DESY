import { create } from "apisauce";
import store from "../store";
export const dispatchToReducer = (type, payload) => {
  return {
    type,
    payload
  };
};

export const api = () => {
  let jwt = store.getState().auth.jwt;
  return create({
    baseURL: window.location.origin + "/api",
    headers: {
      Authorization: `Bearer ${jwt}`
    }
  });
};

export const oktaApi = () => {
  return create({
    baseURL: process.env.REACT_APP_OKTA_BASE_URL + "/api/v1",
    headers: {
      Authorization: `SSWS ${process.env.REACT_APP_OKTA_API_TOKEN}`
    }
  });
};
