import { api, dispatchToReducer } from "./actionUtils";

export const CREATE_REQUEST = "CREATE_REQUEST";
export const CREATE_REQUEST_SUCCESS = "CREATE_REQUEST_SUCCESS";
export const CREATE_REQUEST_FAILURE = "CREATE_REQUEST_FAILURE";
export const CREATE_REQUEST_CLEAR = "CREATE_REQUEST_CLEAR";
export const RETRIEVE_ALL_REQUESTS = "RETRIEVE_ALL_REQUESTS";
export const RETRIEVE_ALL_REQUEST_SUCCESS = "RETRIEVE_ALL_REQUESTS_SUCCESS";
export const RETRIEVE_ALL_REQUESTS_FAILURE = "RETRIEVE_ALL_REQUESTS_FAILURE";
export const RETRIEVE_PAST_REQUEST = "RETRIEVE_PAST_REQUEST";
export const RETRIEVE_PAST_REQUEST_SUCCESS = "RETRIEVE_PAST_REQUEST_SUCCESS";
export const RETRIEVE_PAST_REQUEST_FAILURE = "RETRIEVE_PAST_REQUEST_FAILURE";
export const RETRIEVE_OPEN_REQUEST = "RETRIEVE_OPEN_REQUEST";
export const RETRIEVE_OPEN_REQUEST_SUCCESS = "RETRIEVE_OPEN_REQUEST_SUCCESS";
export const RETRIEVE_OPEN_REQUEST_FAILURE = "RETRIEVE_OPEN_REQUEST_FAILURE";
export const UPDATE_REQUEST = "UPDATE_REQUEST";
export const UPDATE_REQUEST_WAIT = "UPDATE_REQUEST_WAIT";
export const UPDATE_REQUEST_WAIT_SUCCESS = "UPDATE_REQUEST_WAIT_SUCCESS";
export const UPDATE_REQUEST_WAIT_FAILURE = "UPDATE_REQUEST_WAIT_FAILURE";
export const UPDATE_REQUEST_SUCCESS = "UPDATE_REQUEST_SUCCESS";
export const UPDATE_REQUEST_FAILURE = "UPDATE_REQUEST_FAILURE";
export const UPDATE_ALL_REQUEST = "UPDATE_ALL_REQUEST";
export const UPDATE_ALL_REQUEST_SUCCESS = "UPDATE_ALL_REQUEST_SUCCESS";
export const UPDATE_ALL_REQUEST_FAILURE = "UPDATE_ALL_REQUEST_FAILURE";
export const DETAILED_REQUEST = "DETAILED_REQUEST";
export const DETAILED_REQUEST_SUCCESS = "DETAILED_REQUEST_SUCCESS";
export const DETAILED_REQUEST_FAILURE = "DETAILED_REQUEST_FAILURE";

export const createRequest = request => {
  return dispatch => {
    dispatch(dispatchToReducer(CREATE_REQUEST));
    api()
      .post("/requests", request)
      .then(res => {
        const data = res.data;
        if (res.ok) {
          dispatch(dispatchToReducer(CREATE_REQUEST_SUCCESS, data));
        } else {
          dispatch(dispatchToReducer(CREATE_REQUEST_FAILURE, data));
        }
      });
  };
};

export const getAllRequests = id => {
  return dispatch => {
    dispatch(dispatchToReducer(RETRIEVE_ALL_REQUESTS));
    api()
      .get(`/requests/${id}/all`)
      .then(res => {
        const data = res.data;
        if (res.ok) {
          dispatch(dispatchToReducer(RETRIEVE_ALL_REQUEST_SUCCESS, data));
        } else {
          dispatch(dispatchToReducer(RETRIEVE_ALL_REQUESTS_FAILURE, data));
        }
      });
  };
};

export const getAllOpenRequests = id => {
  return dispatch => {
    dispatch(dispatchToReducer(RETRIEVE_OPEN_REQUEST));
    api()
      .get(`/requests/${id}/all/open`)
      .then(res => {
        const data = res.data;
        if (res.ok) {
          dispatch(dispatchToReducer(RETRIEVE_OPEN_REQUEST_SUCCESS, data));
        } else {
          dispatch(dispatchToReducer(RETRIEVE_OPEN_REQUEST_FAILURE, data));
        }
      });
  };
};

export const getAllPastRequests = id => {
  return dispatch => {
    dispatch(dispatchToReducer(RETRIEVE_PAST_REQUEST));
    api()
      .get(`/requests/${id}/all/past`)
      .then(res => {
        const data = res.data;
        if (res.ok) {
          dispatch(dispatchToReducer(RETRIEVE_PAST_REQUEST_SUCCESS, data));
        } else {
          dispatch(dispatchToReducer(RETRIEVE_PAST_REQUEST_FAILURE, data));
        }
      });
  };
};

export const updateAll = userId => {
  return dispatch => {
    dispatch(dispatchToReducer(UPDATE_ALL_REQUEST));
    api()
      .post(`/requests/update`, { userId: userId })
      .then(res => {
        dispatch(
          dispatchToReducer(
            res.ok ? UPDATE_ALL_REQUEST_SUCCESS : UPDATE_ALL_REQUEST_FAILURE,
            res.data
          )
        );
      });
  };
};

export const updateAndWait = userId => {
  return dispatch => {
    dispatch(dispatchToReducer(UPDATE_REQUEST_WAIT));
    api()
      .post(`/requests/updateAndWait`, { userId: userId })
      .then(res => {
        dispatch(
          dispatchToReducer(
            res.ok ? UPDATE_REQUEST_WAIT_SUCCESS : UPDATE_REQUEST_WAIT_FAILURE,
            res.data
          )
        );
      });
  };
};

export const updateJob = jobId => {
  return dispatch => {
    dispatch(dispatchToReducer(UPDATE_REQUEST));
    api()
      .post(`/updateOne`, { jobId: jobId })
      .then(res => {
        const data = res.data;
        if (res.ok) {
          dispatch(dispatchToReducer(UPDATE_REQUEST_SUCCESS, data));
        } else {
          dispatch(dispatchToReducer(UPDATE_REQUEST_FAILURE, data));
        }
      });
  };
};

export const getDetails = jobId => {
  return dispatch => {
    dispatch(dispatchToReducer(DETAILED_REQUEST));
    api()
      .get(`/jobDetails/${jobId}`)
      .then(res => {
        const data = res.data;
        if (res.ok) {
          dispatch(dispatchToReducer(DETAILED_REQUEST_SUCCESS, data));
        } else {
          dispatch(dispatchToReducer(DETAILED_REQUEST_FAILURE, data));
        }
      });
  };
};

export const clear = () => ({
  type: CREATE_REQUEST_CLEAR
});
