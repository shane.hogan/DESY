import OktaAuth from "@okta/okta-auth-js";
import { dispatchToReducer, oktaApi } from "./actionUtils";
import { updateAll } from "./RequestActions";
export const JWT_TOKEN = "JWT_TOKEN";
export const LOGIN = "LOGIN";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";
export const LOGOUT = "LOGOUT";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const UPDATE_AUTHENTICATED = "UPDATE_AUTHENTICATED";
export const USER_ROLE = "USER_ROLE";

const oktaAuth = new OktaAuth({
  url: process.env.REACT_APP_OKTA_BASE_URL,
  clientId: process.env.REACT_APP_OKTA_CLIENT_ID,
  redirectUri: process.env.REACT_APP_OKTA_REDIRECT_URI
});

export const checkAuth = auth => {
  return dispatch => {
    auth.isAuthenticated().then(authenticated => {
      dispatch(updateAuthenticated(authenticated));
    });
  };
};

export const updateAuthenticated = authenticated => ({
  type: UPDATE_AUTHENTICATED,
  payload: authenticated
});

export const login = credentials => {
  return dispatch => {
    dispatch(dispatchToReducer(LOGIN));
    oktaAuth
      .signIn(credentials)
      .then(res => {
        dispatch(dispatchToReducer(LOGIN_SUCCESS, res));
        dispatch(getGroup(res.user.id));
        dispatch(updateAll(res.user.id));
      })
      .catch(err => {
        dispatch(dispatchToReducer(LOGIN_FAILURE, err));
      });
  };
};

export const setJwt = idToken => {
  return dispatch => {
    dispatch(dispatchToReducer(JWT_TOKEN, idToken));
  };
};

export const getGroup = userId => {
  return dispatch => {
    oktaApi()
      .get(`/users/${userId}/groups`)
      .then(response => {
        dispatch(dispatchToReducer(USER_ROLE, response.data));
      });
  };
};

export const logout = auth => {
  return dispatch => {
    dispatch(dispatchToReducer(LOGOUT));
    auth
      .logout("/")
      .then(() => {
        dispatch(dispatchToReducer(LOGOUT_SUCCESS));
      })
      .catch(() => {
        dispatch(dispatchToReducer(LOGOUT_SUCCESS));
      });
  };
};
