import React from "react";
import Charts from "../containers/ChartContainer";
import Helper from "../components/Helper";
import LayoutHelper from "./LayoutHelper";

const helper = () => (
  <Helper header={"ETL Reports"}>
    Unlike to the <i>ETL Request Statues</i> page, the following reports are generated
    using <strong>ALL</strong> submitted records, not just the current users.
  </Helper>
);

export default class ReportsPage extends React.Component {
  render() {
    return (
      <LayoutHelper pageTitle={"ETL Reports"} helper={helper()}>
        <Charts />
      </LayoutHelper>
    );
  }
}
