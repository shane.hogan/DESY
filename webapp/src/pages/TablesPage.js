import React from "react";
import { Col, Row } from "reactstrap";
import DbSchema from "../components/DbSchema";
import * as _ from "lodash";
import Helper from "../components/Helper";
import LayoutHelper from "./LayoutHelper";

const helper = () => (
  <Helper header={"ETL Catalogs"}>
    Each table shown in this screen represents the Schema for a table The very top row (in
    bold text) is the description of that schema, and each following row describes a
    column. The column name is on the left and the column data type is on the right.
    <br />
    <br />
    These schemas are fetched through a series of api calls. The client makes a request to
    our express.js REST API, which verifies the users authorization and then securely
    connects to AWS with several secret keys. The data AWS returns is parsed and then
    returned to the client.
  </Helper>
);

class TablesPage extends React.Component {
  componentWillMount() {
    this.props.getTables();
  }

  render() {
    const { tables } = this.props;
    let sortedTables = _.orderBy(
      tables,
      [
        function(t) {
          return t.columns.length;
        }
      ],
      "desc"
    );
    return (
      <LayoutHelper pageTitle={"ETL Catalogs"} helper={helper()}>
        <Row>
          {tables &&
            sortedTables.length > 0 &&
            sortedTables.map(table => {
              return (
                <Col
                  xs={12}
                  sm={6}
                  md={6}
                  lg={4}
                  key={table.description}
                  style={{ paddingBottom: "3rem" }}
                >
                  <DbSchema header={table.description} columns={table.columns} />
                </Col>
              );
            })}
        </Row>
      </LayoutHelper>
    );
  }
}

export default TablesPage;
