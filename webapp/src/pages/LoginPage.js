import React from "react";
import LoginForm from "../forms/LoginForm";
import LoginFormUsernameOnly from "../forms/LoginForm_UsernameOnly";
import { withRouter } from "react-router";
import { withAuth } from "@okta/okta-react";
import { Alert, Col, Container, Row } from "reactstrap";
import { Card, CardBody, ModalFooter } from "mdbreact";
import Helper from "../components/Helper";
import OktaAuth from "@okta/okta-auth-js/lib/index";

const useSimpleLogin = process.env.REACT_APP_USE_SIMPLE_LOGIN;

class LoginPage extends React.Component {
  async componentDidUpdate(prevProps) {
    if (this.props.authenticated && !prevProps.authenticated) {
      const oktaAuth = new OktaAuth({
        url: process.env.REACT_APP_OKTA_BASE_URL,
        clientId: process.env.REACT_APP_OKTA_CLIENT_ID,
        redirectUri: process.env.REACT_APP_OKTA_REDIRECT_URI
      });
      oktaAuth.token.getWithRedirect({
        responseType: ["id_token", "token"],
        scopes: ["openid", "email", "profile", "address", "phone", "groups"],
        sessionToken: this.props.sessionToken
      });
    }
  }

  render() {
    const { formErrors } = this.props;
    return (
      <Container>
        <Helper header={"Authentication & Security"}>
          <strong>To Login</strong> use the username <strong>cmsuser1</strong> and click{" "}
          <i>SIGN IN</i>
          <br />
          <br />
          For authentication we are using Okta as our security infrastructure and
          utilizing several Okta libraries to secure the application. In order to maintain
          simplicity for demo purposes, we only ask for a username and then if that
          matches <strong>cmsuser1</strong> we send a correct set of username/password to
          Okta to crate a session. A JWT (<i>JSON Web Token</i>) is also returned that we
          use to authenticate api calls, and keep track of ETL Requests submitted by each
          user.
        </Helper>
        <Row style={{ marginTop: "1rem", marginBottom: "5rem" }}>
          <Col sm={12} md={{ size: 8, offset: 2 }} lg={{ size: 6, offset: 3 }}>
            {formErrors && <Alert color={"danger"}>{formErrors.message}</Alert>}
            <Card>
              <CardBody>
                <div className="text-center">
                  <h3 className="dark-grey-darken-1 mb-5">
                    <strong>Sign in</strong>
                  </h3>
                </div>
                {useSimpleLogin && useSimpleLogin.toString() === "true" ? (
                  <LoginFormUsernameOnly
                    id={"login-form-card"}
                    onSubmit={this.props.login_usernameOnly.bind(this)}
                  />
                ) : (
                  <LoginForm onSubmit={this.props.login.bind(this)} />
                )}
              </CardBody>
              <ModalFooter className="mx-5 pt-3 mb-1">
                <Row className={"text-right"}>
                  <Col xs={12}>
                    Forgot <span className="blue-text ml-1"> Username?</span>
                  </Col>
                  <Col xs={12}>
                    Don't Have an Account?{" "}
                    <span className="blue-text ml-1"> Sign Up</span>
                  </Col>
                </Row>
              </ModalFooter>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default withRouter(withAuth(LoginPage));
