import React from "react";
import { Alert, Col } from "reactstrap";
import { Button, Fa } from "mdbreact";
import { RequestStatusTable } from "../components/RequestStatusTable";
import Helper from "../components/Helper";
import LayoutHelper from "./LayoutHelper";

const helper = () => (
  <Helper header={"ETL Request Statuses"}>
    The table in the screen shows any and all ETL Requests that the{" "}
    <strong>CURRENT</strong> user has submitted, along with their current statuses. In
    case of a large amount of records, the user can sort, and filter results by utilizing
    the filters in the header row.
    <br />
    <br />
    The statuses are automatically updated every time a user logs into the system, however
    a user can also manually trigger a refresh by clicking the refresh (<Fa
      icon={"refresh"}
    />) icon above the table.
  </Helper>
);

class RequestsStatusPage extends React.Component {
  componentWillMount() {
    this.props.retrieveAllRequests(this.props.userId);
  }

  render() {
    const { requests, errors, updateAndWait, userId, updating } = this.props;
    return (
      <LayoutHelper pageTitle={"ETL Request Statuses"} helper={helper()}>
        {errors && <Alert color={"danger"}>{errors.message}</Alert>}
        {requests && (
          <div>
            <Col style={{ textAlign: "right" }}>
              <Button
                outline
                color="primary"
                aria-label={"refresh"}
                style={{ borderRadius: "100px" }}
                onClick={updateAndWait.bind(this, userId)}
              >
                <Fa icon="refresh" spin={updating} />
              </Button>
            </Col>
            <RequestStatusTable data={requests} />
          </div>
        )}
      </LayoutHelper>
    );
  }
}

export default RequestsStatusPage;
