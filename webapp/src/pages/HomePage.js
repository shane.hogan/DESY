import React from "react";
import { Container } from "reactstrap";
import { Redirect, withRouter } from "react-router";
import { Badge } from "mdbreact";

class HomePage extends React.Component {
  render() {
    const { authenticated, userProfile, roles } = this.props;
    return (
      <Container>
        {authenticated ? (
          <div>
            <h1>Welcome,{formatName(userProfile)}</h1>
            <h5>
              <i>Current Access Roles</i>{" "}
              {roles &&
                roles.map(role => {
                  return (
                    <Badge color={"green"} style={{ margin: "10px" }} key={role.id}>
                      {role.profile.name}
                    </Badge>
                  );
                })}
            </h5>
            {JSON.stringify(userProfile)}
          </div>
        ) : (
          <Redirect to={"/login"} />
        )}
      </Container>
    );
  }
}
const formatName = user => {
  if (user.displayName) return user.dispalyName;
  return `${user.firstName || ""} ${user.lastName || ""}`;
};

export default withRouter(HomePage);
