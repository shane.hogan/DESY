import React from "react";
import { Alert, Col } from "reactstrap";
import CreateRequestForm from "../forms/CreateRequestsForm";
import { Card, CardBody } from "mdbreact";
import Helper from "../components/Helper";
import LayoutHelper from "./LayoutHelper";

const helper = () => (
  <Helper header={"Create ETL Request"}>
    To create a new ETL Request fill out the form on the Screen. The <i>Data Source</i> is
    the source for the ETL data extraction. The <i>File Name</i> is the desired name of
    for the destination file. The <i>ETL Format</i> is the format in which the data should
    be transformed. Lastly, the <i>DUA Number</i> is the Data Use Agreement Number for the
    request. request.
    <br />
    <br />
    The request is sent to our API which validates the data and stores it, along with the
    users user ID, in a mongo database. It will then trigger an AWS Glue (ETL) Job to run
    with the specified parameters. When a response is received from AWS Glue, we will
    update that saved item with the new status and newly generated Job Runner Id (from AWS
    Glue). A success/fail response will then be sent to the client.
  </Helper>
);

class CreateRequestsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      errorAlert: false,
      alertOpen: false
    };
  }

  componentWillUpdate(nextProps) {
    if (!this.props.request && nextProps.request) {
      this.setState({ alertOpen: true });
    }
    if (!this.props.formErrors && nextProps.formErrors) {
      this.setState({ errorAlert: true });
    }
    if (this.props.reqForm && this.props.reqForm.values && !nextProps.reqForm.values) {
      this.setState({ errorAlert: false });
    }
  }

  render() {
    const { formErrors, createRequest, userId } = this.props;
    return (
      <LayoutHelper pageTitle={"Create ETL Request"} helper={helper()}>
        <Alert color={"danger"} isOpen={this.state.errorAlert}>
          {formErrors &&
            (formErrors.message || "Something went wrong. ETL Request failed to submit")}
        </Alert>
        <Alert color={"success"} isOpen={this.state.alertOpen}>
          ETL Request submitted successfully.
        </Alert>
        <Col sm={12} md={{ size: 8 }} style={{ marginBottom: "5rem" }}>
          <Card>
            <CardBody>
              <div className="text-center">
                <h3 className="dark-grey-text mb-5">
                  <strong>Create New ETL Request</strong>
                </h3>
              </div>
              <CreateRequestForm onSubmit={createRequest.bind(this, userId)} />
            </CardBody>
          </Card>
        </Col>
      </LayoutHelper>
    );
  }
}

export default CreateRequestsPage;
