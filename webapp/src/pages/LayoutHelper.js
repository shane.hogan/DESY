import React from "react";
import { Col, Container, Row } from "reactstrap";

const LayoutHelper = props => {
  const { pageTitle, helper, children } = props;
  return (
    <Container fluid>
      <Row style={{ marginTop: "1rem" }}>
        {pageTitle && (
          <Col xs={12} sm={10} md={{ size: 9, offset: 3 }}>
            <h1>{pageTitle}</h1>
          </Col>
        )}
        <Row style={{ marginTop: "1rem", marginBottom: "5rem", width: "100%" }}>
          <Col xs={12} sm={12} lg={3} className={"mx-auto"}>
            {helper}
          </Col>
          <Col xs={12} md={12} lg={9} className={"mx-auto"}>
            {children}
          </Col>
        </Row>
      </Row>
    </Container>
  );
};

export default LayoutHelper;
