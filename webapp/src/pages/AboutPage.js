import React from "react";
import { Col, Container, Nav, NavItem, NavLink, TabContent, TabPane } from "reactstrap";
import Technologies from "../components/Technologies";
import { Button, Card, CardBody, CardHeader, CardText, Row } from "mdbreact";
import { UserStories } from "../components/UserStories";
import { DigitalService } from "../components/DigitalService";
import { TechnologiesConsidered } from "../components/TechnologiesConsidered";
import Backlog from "../components/Backlog";
import DemoVideo from "../components/DemoVideo";
import Diagrams from "../components/Diagrams";

const tabs = [
  {
    header: "Diagrams",
    component: <Diagrams />
  },
  {
    header: "Video",
    component: <DemoVideo />
  },
  {
    header: "Technologies Used",
    component: <Technologies />
  },
  {
    header: "Technologies Considered",
    component: <TechnologiesConsidered />
  },
  {
    header: "User Stories",
    component: <UserStories />
  },
  {
    header: "Digital Service Plays",
    component: <DigitalService />
  },
  {
    header: "Backlog",
    component: <Backlog />
  }
];

class AboutPage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { activeTab: tabs[0].header };
  }

  toggle(chartName) {
    this.setState({ activeTab: chartName });
  }

  render() {
    return (
      <Container fluid>
        <Container className={"my-5"}>
          <h1 className={"text-center"}>
            Welcome to the clearAvenue ETLaaS Prototype Demonstration
          </h1>
          <Card>
            <CardHeader>
              <h4>Overview</h4>
            </CardHeader>
            <CardBody>
              <CardText>
                The intent of Team clearAvenue’s prototype is to showcase our solution,
                development and operational processes. In this demonstration, we show ETL
                as a Service (ETLaaS) utilizing a responsive UI framework based on
                React/Redux and Amazon Glue and related components hosted in the AWS
                cloud. Based on CMS’s requirements and our technical team’s analysis of
                the SOW and related material, we propose the prototype solution shown in
                Exhibit 4 of our proposal response. Our solution is designed to be agile
                and robust, yet still be cost efficient. The prototype solution will be
                refined after contract award when CMS guidance and requirements are
                elicited by our team by the various CMS stakeholders.
              </CardText>
            </CardBody>
          </Card>
          <Row className={"justify-content-center my-2"}>
            <Col xs={12} md={4}>
              <Button
                gradient="blue"
                rounded
                className="btn-block z-depth-1a"
                onClick={() =>
                  window.open("https://gitlab.com/shane.hogan/ETLaaS", "_blank")
                }
              >
                View Source Code on GitLab
              </Button>
            </Col>
          </Row>
        </Container>
        <Container>
          {tabs && (
            <Nav tabs>
              {tabs.map(tab => {
                return (
                  <NavItem key={`navItem_${tab.header}`}>
                    <NavLink
                      className={this.state.activeTab === tab.header ? "active" : ""}
                      onClick={this.toggle.bind(this, tab.header)}
                    >
                      {tab.header}
                    </NavLink>
                  </NavItem>
                );
              })}
            </Nav>
          )}
        </Container>
        <Container fluid>
          {tabs && (
            <TabContent activeTab={this.state.activeTab}>
              {tabs.map(tab => {
                return (
                  <TabPane tabId={tab.header} key={`tabPane_${tab.header}`}>
                    {tab.component}
                  </TabPane>
                );
              })}
            </TabContent>
          )}
        </Container>
      </Container>
    );
  }
}

export default AboutPage;
