import CreateRequestsPage from "../pages/CreateRequestsPage";
import { connect } from "react-redux";
import { createRequest } from "../actions/RequestActions";

const mapStateToProps = state => ({
  isLoading: state.requests.isLoading,
  formErrors: state.requests.error,
  request: state.requests.addedRequest,
  userId: state.auth.user && state.auth.user.id,
  reqForm: state.form.createRequestsForm
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  createRequest: (userId, values) => {
    values.userId = userId;
    dispatch(createRequest(values));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateRequestsPage);
