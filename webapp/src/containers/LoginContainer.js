import LoginPage from "../pages/LoginPage";
import { connect } from "react-redux";
import { login } from "../actions/authActions";

const mapStateToProps = state => ({
  authenticated: state.auth.authenticated,
  formErrors: state.auth.error,
  sessionToken: state.auth.sessionToken
});

const mapDispatchToProps = dispatch => ({
  login: values => {
    dispatch(login(values));
  },
  login_usernameOnly: values => {
    easyLogin(values, dispatch);
  }
});

/**
 * For demo purpose only!
 * If correct username send okta a correct set of credentials, otherwsise send bad credentials
 * @param values
 * @param dispatch
 */
const easyLogin = (values, dispatch) => {
  if (values.username && values.username === process.env.REACT_APP_DEFAULT_USERNAME) {
    dispatch(
      login({
        username: process.env.REACT_APP_USERNAME,
        password: process.env.REACT_APP_PASSWORD
      })
    );
  } else {
    dispatch(login({ username: "bad username", password: "bad password" }));
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
