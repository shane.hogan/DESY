import OpenRequests from "../pages/RequestsStatusPage";
import { connect } from "react-redux";
import {
  getAllOpenRequests,
  getAllRequests,
  updateAll,
  updateAndWait
} from "../actions/RequestActions";

const mapStateToProps = state => ({
  errors: state.requests.error,
  requests: state.requests.requests,
  userId: state.auth.user.id,
  updating: state.requests.updating
});

const mapDispatchToProps = dispatch => ({
  retrieveOpenRequests: id => {
    dispatch(getAllOpenRequests(id));
  },
  retrieveAllRequests: id => {
    dispatch(getAllRequests(id));
  },
  updateAll: id => {
    dispatch(updateAll(id));
  },
  updateAndWait: id => {
    dispatch(updateAndWait(id));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OpenRequests);
