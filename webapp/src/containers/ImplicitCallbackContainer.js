import ImplicitCallback from "../components/ImplicitCallback";
import { connect } from "react-redux";
import { setJwt } from "../actions/authActions";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  setJwt: idToken => {
    dispatch(setJwt(idToken));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ImplicitCallback);
