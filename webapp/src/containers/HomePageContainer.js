import HomePage from "../pages/HomePage";
import { connect } from "react-redux";

const mapStateToProps = state => ({
  authenticated: state.auth.authenticated,
  userProfile: state.auth.user && state.auth.user.profile,
  roles: state.auth.roles
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);
