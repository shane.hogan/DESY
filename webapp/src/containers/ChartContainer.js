import Charts from "../components/Chart";
import { connect } from "react-redux";
import { getAllCharts } from "../actions/chartActions";

const mapStateToProps = state => ({
  allCharts: state.charts.allCharts,
  execAverages: state.charts.execAverages,
  dailyTotals: state.charts.dailyTotals,
  statusTotals: state.charts.statusTotals,
  dataSourceTotals: state.charts.dataSourceTotals
});

const mapDispatchToProps = dispatch => ({
  getAllCharts: () => {
    dispatch(getAllCharts());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Charts);
