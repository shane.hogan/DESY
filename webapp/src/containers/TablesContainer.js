import Tables from "../pages/TablesPage";
import { connect } from "react-redux";
import { getTables } from "../actions/tableActions";

const mapStateToProps = state => ({
  errors: state.requests.error,
  tables: state.tables.tables
});

const mapDispatchToProps = dispatch => ({
  getTables: id => {
    dispatch(getTables(id));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tables);
