import AuthButton from "../components/AuthButton";
import { connect } from "react-redux";
import { logout } from "../actions/authActions";

const mapStateToProps = state => ({
  authenticated: state.auth.authenticated,
  user: state.auth.user
});

const mapDispatchToProps = dispatch => ({
  logout: auth => {
    dispatch(logout(auth));
  },
  login: history => {
    history.push("/login");
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthButton);
