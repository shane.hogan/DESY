import NavHeader from "../components/NavHeader";
import { connect } from "react-redux";

const mapStateToProps = state => ({
  authenticated: state.auth.authenticated || false,
  user: state.auth.user,
  userProfile: state.auth.user && state.auth.user.profile,
  roles: state.auth.roles
});

const mapDispatchToProps = dispatch => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NavHeader);
