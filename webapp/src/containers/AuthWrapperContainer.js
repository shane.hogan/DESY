import AuthWrapper from "../components/AuthWrapper";
import { connect } from "react-redux";
import { logout } from "../actions/authActions";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  logout: auth => {
    dispatch(logout(auth));
  },
  login: history => {
    history.push("/login");
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthWrapper);
