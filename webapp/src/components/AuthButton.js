import React from "react";
import { withAuth } from "@okta/okta-react/dist/index";
import { withRouter } from "react-router";

class AuthButton extends React.Component {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
    this.login = this.login.bind(this);
  }

  async logout() {
    const auth = await this.props.auth;
    this.props.logout(auth);
  }

  login() {
    this.props.login(this.props.history);
  }

  render() {
    const { nav, authenticated } = this.props;
    const isNav = nav ? nav === "true" || nav === true : false;
    const isAuthenticated = authenticated
      ? authenticated === true || authenticated === "true"
      : false;
    if (isNav) {
      return (
        <a onClick={!isAuthenticated ? this.login : this.logout} className={"nav-link"}>
          {!isAuthenticated ? "Login" : "Logout"}
        </a>
      );
    } else
      return (
        <button onClick={!isAuthenticated ? this.login : this.logout}>
          {!isAuthenticated ? "Login" : "Logout"}
        </button>
      );
  }
}

export default withRouter(withAuth(AuthButton));
