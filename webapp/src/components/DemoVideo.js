import React from "react";
import {
  Player,
  ControlBar,
  PlaybackRateMenuButton,
  ReplayControl,
  BigPlayButton
} from "video-react";
import { Container } from "reactstrap";
import demoVideo from "../media/demo-video-take1.mp4";
import splash from "../media/demo-video-splash.png";
import { Card } from "mdbreact";

const DemoVideo = () => (
  <Container style={{ marginBottom: "5rem" }} className={"my-5"}>
    <Card>
      <Player src={demoVideo} poster={splash}>
        <ControlBar autoHide={false}>
          <BigPlayButton position="center" />
          <ReplayControl seconds={10} order={2.2} />
          <PlaybackRateMenuButton rates={[5, 3, 1.5, 1, 0.5, 0.1]} order={7.1} />
        </ControlBar>
      </Player>
    </Card>
  </Container>
);

export default DemoVideo;
