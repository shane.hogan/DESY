import React from "react";
import { Col, Container, Row } from "reactstrap";
import { UserStory } from "./UserStory";

export const UserStories = () => {
  return (
    <Container>
      <section className="my-3">
        <h2 className="h1-responsive font-weight-bold my-5 text-center">User Stories</h2>
        <Row style={{ marginTop: "1rem" }}>
          {stories.map(story => {
            return (
              <Col xs={12} md={6} key={"story_" + story.title}>
                <UserStory story={story} />
              </Col>
            );
          })}
        </Row>
      </section>
    </Container>
  );
};

const stories = [
  {
    title:
      "#1: As a system user, I need to log in to the system so that I can access the appropriate content.",
    description:
      "Users must provide their login and password to access the system.  Provided data will be validated by the system and, if matching, will present a Dashboard page to the user.  In the event there is not a match, the user will be displayed an error message and presented with the login and password prompts.",
    acceptanceCriteria: [
      "Provide prompt for User Id.",
      "Provide prompt for Password.",
      "Allow the user to submit the entries.",
      "If User Id and Password match, then present a Dashboard page.",
      "Dashboard page should say, “Welcome <<User Id>>”.",
      "If the User Id and Password do not match, remain on the login page and display “Error: Invalid User ID/Password.”"
    ]
  },
  {
    title:
      "#2: As the ETLaaS system, I need to access the Enterprise Privacy Policy (EPPE) system to retrieve user Data Use Agreement (DUA) ",
    description:
      "A user’s DUA information resides in EPPE and is needed for the login & authentication process.  An interface between ETLaaS and EPPE is needed to access the DUA data.",
    acceptanceCriteria: [
      "Given a User ID and a matching password, ETLaaS should be able to access that person’s related DUA information in EPPE.",
      "Login, access and information return must occur real time within an acceptable timeframe*."
    ],
    note:
      "May require a user story to match and apply permissions with the result of the EPPE interface."
  },
  {
    title:
      "#3: As a ETLaaS user, I need to submit a request for an existing extract to be run.",
    description:
      "Users should be able to select an extract to be executed by the ETLaaS system.",
    acceptanceCriteria: [
      "User is presented with the available ETL instructions which access a supported data source.",
      "User can select a set of ETL instructions.",
      "User can submit the request to be processed."
    ],
    note:
      "This user story focuses on the capability to select a set of ETL instructions to be processed.  User Story 4 will address the need to configure extract instructions through the user interface."
  },
  {
    title:
      "#4: As a ETLaaS user, I need to create a new extract from an existing data source. ",
    description:
      "Users should be able to select a data source that is allowable through their DUA for extraction and supported by ETLaaS and specify the data to be included in the extract.",
    acceptanceCriteria: [
      "User should be able to select a supported data source.",
      "User should be able to select the fields to be included in the extract.",
      "User should be able to specify any instructions with regard to the extract.",
      "The complete set of instructions should be persisted in ETLaaS.",
      "Users can submit the request to be processed."
    ]
  },
  {
    title:
      "#5: As a ETLaaS user, I need to create a new extract from a new data source. ",
    description:
      "Users should be able to request a new data source to be supported by ETLaaS.",
    acceptanceCriteria: [
      "User should have a mechanism by which to select a new data source for support."
    ],
    note: "This user story may be a development request."
  },
  {
    title:
      "#6: As a ETLaaS user, I need to be able to check the status of my requested extract runs.",
    description:
      "Users can check on the status of jobs that were submitted through the ETLaaS user interface.  ",
    acceptanceCriteria: [
      "User is presented with each request that was submitted and a status associated with it.",
      "User should dee the summary details related to the request (data source, date/time submitted, status)."
    ]
  },
  {
    title:
      "#7: As a ETLaaS user, I need to see the data elements available in the supported data sources.",
    description:
      "Supported data sources should have meta data available and visible through the ETLaaS application. ",
    acceptanceCriteria: [
      "User should be able to view all data sources available in accordance with their ETLaaS access rights and DUA permissions.",
      "User should be able to see the data elements that are available in the available data sources.",
      "User should be able to see the data type associated with each element that is available."
    ]
  },
  {
    title: "#8: As the ETLaaS system, I need to initiate and run an extract request.",
    description:
      "Users should be able to view and select previously saved and valid extract instructions and initiate them at different times.",
    acceptanceCriteria: [
      "Users should be able to view all previously submitted extract requests made through ETLaaS.",
      "Users should be able to select previously saved request details.",
      "Users should be able to initiate extract requests on previous extract requests for those that are still in compliance with the DUA agreement."
    ],
    note:
      "This could potentially be rolled up into acceptance criteria of other user stories, as the intent, in most cases, will be to run the extract.  This will be kept for the scenario where a rerun is intended, or a previously persisted set of instructions is run at a separate time."
  },
  {
    title:
      "#9: As the ETLaaS system, I need to deliver a data file, as specified by the user.",
    description:
      "Users can select the delivery and encryption method for requested extracts.",
    acceptanceCriteria: [
      "Data extract results should be delivered per the user specified instructions.",
      "Data should conform to the encryption method specified by the user.",
      "Data should be delivered in the format that is specified by the user."
    ],
    note:
      "This may be considered an epic, with supporting user stories for each of the possible methods."
  },
  {
    title:
      "#10: As the ETLaaS system, I need to secure and encrypt delivered files that result from an extract.",
    description: "The ETLaaS system should encrypt data extracts.",
    acceptanceCriteria: [
      "Extract file or data should be delivered in a secured manner, conforming to encryption standards."
    ],
    note:
      "This This is a companion user story to #9, where that story focuses on delivering in accordance to the user specifications, this story focuses on the technical ability to encrypt a file."
  },
  {
    title:
      "#11: As the ETLaaS system, I need to deliver extract results to a database environment.",
    description:
      "Users need to have the results of an extract request delivered to a table structure in a secured environment.",
    acceptanceCriteria: [
      "Data extract results should be delivered to a predefined table structure within the ETLaaS environment.",
      "Data elements should be persisted in accordance with the source – target mapping.",
      "Data should follow security."
    ]
  },
  {
    title:
      "#12: As the ETLaaS system, I need to integrate with the Recovery Management and Accounting System (ReMAS) to receive automated requests for claims data.",
    description:
      "The ReMAS interface provides am automated means for users to request claims data for those users who have a DUA that supports the permissions.",
    acceptanceCriteria: [
      "ETLaaS can receive a request for data from the ReMAS system.",
      "ETLaaS can send status information to the ReMAS system."
    ]
  },
  {
    title:
      "#13: As the ETLaaS system, I need to integrate with the National Claims History (NCH) data repository to provide the user with extract capabilities.",
    description: "ETLaaS system should support extracts from NCH",
    acceptanceCriteria: [
      "ETLaaS should be able to access metadata from NCH.",
      "ETLaaS should be able to submit extract instructions from NCH.",
      "ETLaaS should be able to execute instructions against NCH."
    ]
  },
  {
    title:
      "#14: As the ETLaaS system, I need to integrate with the National Medicare Utilization Database (NMUD) data repository to provide the user with extract capabilities.",
    description: "ETLaaS system should support extracts from NMUD",
    acceptanceCriteria: [
      "ETLaaS should be able to access metadata from NMUD.",
      "ETLaaS should be able to submit extract instructions from NMUD.",
      "ETLaaS should be able to execute instructions against NMUD."
    ]
  },
  {
    title:
      "#15: As the ETLaaS system, I need to integrate with the Medicare Provider Analysis and Review (MEDPAR) data repository to provide the user with extract capabilities.",
    description: "ETLaaS system should support extracts from MEDPAR",
    acceptanceCriteria: [
      "ETLaaS should be able to access metadata from MEDPAR.",
      "ETLaaS should be able to submit extract instructions from MEDPAR.",
      "ETLaaS should be able to execute instructions against MEDPAR."
    ]
  },
  {
    title:
      "#16: As the ETLaaS system, I need to integrate with the Standard Analytical Files (SAF) data repository to provide the user with extract capabilities.",
    description: "ETLaaS system should support extracts from SAF",
    acceptanceCriteria: [
      "ETLaaS should be able to access metadata from SAF.",
      "ETLaaS should be able to submit extract instructions from SAF.",
      "ETLaaS should be able to execute instructions against SAF."
    ]
  },
  {
    title:
      "#17: As the ETLaaS system, I need to integrate with the Denominator (DENOM) data repository to provide the user with extract capabilities.",
    description: "ETLaaS system should support extracts from DENOM",
    acceptanceCriteria: [
      "ETLaaS should be able to access metadata from DENOM.",
      "ETLaaS should be able to submit extract instructions from DENOM.",
      "ETLaaS should be able to execute instructions against DENOM."
    ]
  },
  {
    title:
      "#18: As the ETLaaS system, I need to integrate with the Integrated Data Repository (IDR) data repository to provide the user with extract capabilities.",
    description: "ETLaaS system should support extracts from IDR.",
    acceptanceCriteria: [
      "ETLaaS should be able to access metadata from IDR.",
      "ETLaaS should be able to submit extract instructions from IDR.",
      "ETLaaS should be able to execute instructions against IDR."
    ]
  },
  {
    title:
      "#19: As the ETLaaS system, I need to integrate with the Enrollment Data Base (EDB) data repository to provide the user with extract capabilities.",
    description: "ETLaaS system should support extracts from EDB.",
    acceptanceCriteria: [
      "ETLaaS should be able to access metadata from EDB.",
      "ETLaaS should be able to submit extract instructions from EDB.",
      "ETLaaS should be able to execute instructions against EDB."
    ]
  }
];
