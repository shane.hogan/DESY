import React from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { Container } from "reactstrap";
import { ComparisonTableLegend } from "./ComparisonTableLegend";
import { formatter } from "./MeatballFormatter";

export const EtlComparisonTable = () => (
  <Container style={{ marginBottom: "5rem", marginTop: "1rem" }}>
    <h3>ETL</h3>
    <BootstrapTable
      data={data}
      version={"4"}
      condensed={true}
      responsive={true}
      hover={true}
      striped={true}
      trClassName={"longRow"}
    >
      <TableHeaderColumn
        dataAlign={"center"}
        headerAlign={"center"}
        aria-label={"criteria"}
        isKey
        dataField={"criteria"}
        width={"100"}
      />
      <TableHeaderColumn
        headerAlign={"center"}
        dataField={"glue"}
        dataAlign={"center"}
        aria-label={"AWS Glue"}
        dataFormat={formatter}
        width={"100"}
      >
        AWS Glue
      </TableHeaderColumn>
      <TableHeaderColumn
        headerAlign={"center"}
        dataField={"informatica"}
        dataAlign={"center"}
        aria-label={"Informatica Cloud"}
        dataFormat={formatter}
        width={"100"}
      >
        Informatica Cloud
      </TableHeaderColumn>
      <TableHeaderColumn
        headerAlign={"center"}
        dataField={"ssis"}
        dataAlign={"center"}
        aria-label={"Microsoft SSIS"}
        dataFormat={formatter}
        width={"100"}
      >
        Microsoft SSIS
      </TableHeaderColumn>
      <TableHeaderColumn
        headerAlign={"center"}
        dataField={"sap"}
        dataAlign={"center"}
        aria-label={"SAP"}
        dataFormat={formatter}
        width={"100"}
      >
        SAP
      </TableHeaderColumn>
      <TableHeaderColumn
        headerAlign={"center"}
        dataField={"infoSphere"}
        dataAlign={"center"}
        aria-label={"IBM InfoSphere"}
        dataFormat={formatter}
        width={"100"}
      >
        IBM InfoSphere
      </TableHeaderColumn>
      <TableHeaderColumn
        headerAlign={"center"}
        dataField={"odi"}
        dataAlign={"center"}
        aria-label={"Oracle ODI"}
        dataFormat={formatter}
        width={"100"}
      >
        Oracle ODI
      </TableHeaderColumn>
    </BootstrapTable>
    <ComparisonTableLegend
      green={"Easy to support or already supported."}
      yellow={"Can be supported, but requires substantial effort."}
      red={"Very difficult to support."}
    />
  </Container>
);

const data = [
  {
    criteria: "Low Licensing Cost",
    glue: true,
    informatica: "medium",
    ssis: "medium",
    sap: "medium",
    infoSphere: "medium",
    odi: "medium"
  },
  {
    criteria: "Low Complexity of Implementation",
    glue: true,
    informatica: "medium",
    ssis: true,
    sap: "medium",
    infoSphere: "medium",
    odi: "medium"
  },
  {
    criteria: "Enterprise Scalability",
    glue: true,
    informatica: true,
    ssis: true,
    sap: "medium",
    infoSphere: true,
    odi: true
  },
  {
    criteria: "Vibrant Support Community",
    glue: true,
    informatica: true,
    ssis: "medium",
    sap: "medium",
    infoSphere: "medium",
    odi: "medium"
  },
  {
    criteria: "Available to Expertise at Competitive Price",
    glue: true,
    informatica: "medium",
    ssis: true,
    sap: false,
    infoSphere: "medium",
    odi: true
  },
  {
    criteria: "Support Complex Workflow",
    glue: true,
    informatica: true,
    ssis: true,
    sap: true,
    infoSphere: true,
    odi: true
  },
  {
    criteria: "Distributed Clustering to handle Big Data",
    glue: true,
    informatica: true,
    ssis: false,
    sap: "medium",
    infoSphere: false,
    odi: true
  },
  {
    criteria: "Web Based Interfaces with Rich Interfaces",
    glue: "medium",
    informatica: true,
    ssis: true,
    sap: "medium",
    infoSphere: "medium",
    odi: true
  },
  {
    criteria: "FISMA and C&A Guidlines Support",
    glue: true,
    informatica: true,
    ssis: true,
    sap: true,
    infoSphere: "medium",
    odi: true
  },
  {
    criteria: "Support for Security and Role Based Access",
    glue: true,
    informatica: true,
    ssis: true,
    sap: true,
    infoSphere: "medium",
    odi: true
  },
  {
    criteria: "Portal Integration for Personalization",
    glue: true,
    informatica: true,
    ssis: true,
    sap: "medium",
    infoSphere: "medium",
    odi: true
  },
  {
    criteria: "Calendaring",
    glue: "medium",
    informatica: true,
    ssis: true,
    sap: "medium",
    infoSphere: "medium",
    odi: "medium"
  },
  {
    criteria: "JSON Support",
    glue: true,
    informatica: "medium",
    ssis: "-",
    sap: "medium",
    infoSphere: "-",
    odi: "medium"
  },
  {
    criteria: "Ability to Load Legacy Data",
    glue: true,
    informatica: true,
    ssis: true,
    sap: false,
    infoSphere: "medium",
    odi: "medium"
  },
  {
    criteria: "Custom and Ad-Hoc Reporting",
    glue: true,
    informatica: true,
    ssis: true,
    sap: false,
    infoSphere: "medium",
    odi: true
  }
];
