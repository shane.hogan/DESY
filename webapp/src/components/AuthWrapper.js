import React from "react";
import { withAuth } from "@okta/okta-react/dist/index";
import { Container } from "reactstrap";
import store from "../store";
import { logout } from "../actions/authActions";
class AuthWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuthenticated: false,
      user: null
    };
  }

  componentDidMount() {
    this.checkAuth();
  }
  componentDidUpdate(prevProps, prevState) {
    this.checkAuth(prevState);
  }

  async checkAuth(prevState = {}) {
    this.props.auth.isAuthenticated().then(async isAuthenticated => {
      if (this.state.isAuthenticated !== isAuthenticated) {
        this.setState({ isAuthenticated: isAuthenticated });
        if (!this.state.isAuthenticated && prevState.isAuthenticated) {
          const auth = await this.props.auth;
          store.dispatch(logout(auth));
        }
      }
    });
  }

  render() {
    const { children } = this.props;
    return (
      <Container fluid className={"mainContainer"}>
        {children}
      </Container>
    );
  }
}

export default withAuth(AuthWrapper);
