import React from "react";
import { FormFeedback, FormGroup, Input as Select } from "reactstrap";
import { Input } from "mdbreact";
import * as _ from "lodash";
export const renderInput = ({ input, meta, ...custom }) => {
  return (
    <FormGroup style={{ width: "100%" }}>
      <Input {...input} {...custom} aria-label={input.label || input.name} />
      {renderError(meta)}
    </FormGroup>
  );
};

export const renderSelect = ({
  input,
  meta,
  data,
  valueField,
  labelField,
  ...custom
}) => {
  valueField = valueField == null ? "value" : valueField;
  labelField = labelField == null ? "label" : labelField;
  return (
    <FormGroup style={{ width: "100%" }}>
      <Select
        type={"select"}
        {...input}
        {...custom}
        aria-label={input.label || input.name}
      >
        {_.map(data, option => {
          return (
            <option
              value={option[valueField]}
              aria-label={option[valueField]}
              disabled={option.disabled}
              key={option[valueField]}
            >
              {option[labelField]}
            </option>
          );
        })}
      </Select>
      {renderError(meta)}
    </FormGroup>
  );
};

const renderError = meta => {
  const { touched, error } = meta;
  if (!touched) return "";
  return <FormFeedback style={{ display: "inherit" }}>{error}</FormFeedback>;
};
