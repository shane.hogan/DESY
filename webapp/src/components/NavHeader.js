import React from "react";
import {
  Collapse,
  Container,
  DropdownMenu,
  DropdownToggle,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  UncontrolledDropdown
} from "reactstrap";
import { NavHashLink as NavLink } from "react-router-hash-link";
import AppProperties from "../properties";
import _ from "lodash";
import { withAuth } from "@okta/okta-react/dist/index";
import AuthButton from "../containers/AuthButtonContainer";
import { withRouter } from "react-router";

class NavHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  renderMenuItem = menuItem => {
    if (menuItem.menuItems) {
      return (
        <UncontrolledDropdown nav key={`dropdown__${menuItem.label}`} inNavbar>
          <DropdownToggle nav caret>
            {menuItem.label}
          </DropdownToggle>
          <DropdownMenu right>
            <ul className={"nav"}>
              {_.map(menuItem.menuItems, item => {
                return this.renderMenuItem(item);
              })}
            </ul>
          </DropdownMenu>
        </UncontrolledDropdown>
      );
    } else {
      return (
        <NavItem key={`${menuItem.link}__${menuItem.label}`}>
          <NavLink className={"nav-link"} to={menuItem.link}>
            {menuItem.label}
          </NavLink>
        </NavItem>
      );
    }
  };

  render() {
    const { menuItems, title } = AppProperties;
    const { authenticated } = this.props;
    return (
      <Navbar expand="md" className={"header"}>
        <Container>
          <NavbarBrand href="/">{title}</NavbarBrand>
          <NavbarToggler onClick={this.toggle} aria-label={"nav-bar toggler"} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              {_.map(
                _.filter(menuItems, function(i) {
                  return authenticated ? i.showAuthenticated : i.showUnauthenticated;
                }),
                item => {
                  return this.renderMenuItem(item);
                }
              )}
              <NavItem>
                <AuthButton authenticated={authenticated.toString()} nav={"true"} />
              </NavItem>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    );
  }
}

export default withRouter(withAuth(NavHeader));
