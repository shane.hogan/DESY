import React from "react";
import { Row } from "reactstrap";
import C3Chart from "react-c3js";

const timeSeriesTemplate = (xColumns, dataPoints, yTick) => ({
  chart: {
    data: {
      x: "x",
      columns: [xColumns, ...dataPoints]
    },
    zoom: {
      enabled: false
    },
    axis: {
      x: {
        type: "timeseries",
        tick: {
          format: "%Y-%m-%d"
        }
      },
      y: {
        tick: {
          values: yTick
        }
      }
    }
  }
});

const ChartDailyTotals = props => {
  const { data } = props;
  return (
    <Row>
      {data && (
        <C3Chart {...timeSeriesTemplate(data.dates, data.data, data.yTicks).chart} />
      )}
    </Row>
  );
};

export default ChartDailyTotals;
