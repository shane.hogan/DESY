import React from "react";
import { Col, Container } from "reactstrap";
import framework from "../media/framework.png";
import targetMapping from "../media/targetMapping.png";
import prototypeDiagram from "../media/prototype.png";
import creationFlow from "../media/creationFlow.gif";
import extractFlow from "../media/extractFlow.gif";
import managementProcessFlow from "../media/managementProcessFlow.gif";
import processFlow from "../media/processFlows.gif";

const Diagrams = () => (
  <Container style={{ marginBottom: "5rem", marginTop: "1rem" }}>
    <Col>
      <img src={framework} alt={"Framework Diagram"} className={"responsiveImage"} />
    </Col>
    <hr />
    <Col>
      <img
        src={prototypeDiagram}
        alt={"Prototype Diagram"}
        className={"responsiveImage"}
      />
    </Col>
    <hr />
    <Col>
      <img src={processFlow} alt={"Process Flow Diagram"} className={"responsiveImage"} />
    </Col>
    <hr />
    <Col>
      <img
        src={creationFlow}
        alt={"DUA Creation Flow Diagram"}
        className={"responsiveImage"}
      />
    </Col>
    <hr />
    <Col>
      <img
        src={extractFlow}
        alt={"Data Request and Extract Process Flow Diagram"}
        className={"responsiveImage"}
      />
    </Col>
    <hr />
    <Col>
      <img
        src={managementProcessFlow}
        alt={"Data Extract Management Process Flow Diagram"}
        className={"responsiveImage"}
      />
    </Col>
    <hr />
    <Col>
      <img
        src={targetMapping}
        alt={"Target Mapping Diagram"}
        className={"responsiveImage"}
      />
    </Col>
  </Container>
);

export default Diagrams;
