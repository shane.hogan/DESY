import React from "react";
import { faCheck, faTimesCircle, faDotCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome/index.es";
import { Col, Row } from "reactstrap";

export const ComparisonTableLegend = props => (
  <Row style={{ width: "100%" }}>
    <Col>
      <FontAwesomeIcon icon={faCheck} className="green-text" /> {props.green || "Yes."}
      <FontAwesomeIcon icon={faDotCircle} className="yellow-text" />{" "}
      {props.yellow || "Somewhat."}
      <FontAwesomeIcon icon={faTimesCircle} className="red-text" /> {props.red || "No."}
    </Col>
  </Row>
);
