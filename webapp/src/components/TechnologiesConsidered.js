import React from "react";
import { Container, Row } from "reactstrap";
import { EtlComparisonTable } from "./EtlComparisonTable";
import { ClientComparisonTable } from "./ClientComparisonTable";
import { RestApiComparisonTable } from "./RestApiComparisonTable";

export const TechnologiesConsidered = () => (
  <Container>
    <Row>
      <section className="text-center my-3" style={{ width: "100%" }}>
        <h2 className="h1-responsive font-weight-bold my-5">Technology Comparisons</h2>
        <Row className={"justify-content-md-center"}>
          <EtlComparisonTable />
          <ClientComparisonTable />
          <RestApiComparisonTable />
        </Row>
      </section>
    </Row>
  </Container>
);
