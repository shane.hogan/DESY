import React from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { Container } from "reactstrap";

export const DigitalService = props => {
  return (
    <Container style={{ marginBottom: "5rem" }}>
      <section className="my-3">
        <BootstrapTable
          data={services}
          version={"4"}
          responsive={true}
          hover={true}
          striped={true}
          trClassName={"longRow"}
        >
          <TableHeaderColumn
            dataAlign={"center"}
            headerAlign={"center"}
            aria-label={"id"}
            width={"100"}
            isKey
            dataField={"id"}
          >
            Play
          </TableHeaderColumn>
          <TableHeaderColumn
            headerAlign={"center"}
            dataField={"description"}
            aria-label={"description"}
            width={"30%"}
          >
            Description
          </TableHeaderColumn>
          <TableHeaderColumn
            headerAlign={"center"}
            dataField={"action"}
            aria-label={"action"}
          >
            Team clearAvenue Actions
          </TableHeaderColumn>
        </BootstrapTable>
      </section>
    </Container>
  );
};

const services = [
  {
    id: 1,
    description: "Understand what people need",
    action:
      "We based development of the prototype on our understanding of CMS needs (see examples of User Stories below). These needs will be refined post-award during the kickoff meeting our PM will have with the COR and GTL, as well as subsequent focus groups our technical team will have with potential users of the final system, Our prototype will include a supplemental piece that contains project artifacts such as Product Backlog, User Stories, Sprint Plan, Test Cases, etc."
  },
  {
    id: 2,
    description: "Address the whole experience, from start to finish",
    action:
      "The solution depicted in Exhibit 4 is intended to be a ‘thin’ end-to-end solution that is essentially a “Hello World” that uses technologies and applications. Our technical team will work with CMS to understand the different ways users will interact with the system and their specific reasons for doing so."
  },
  {
    id: 3,
    description: "Make it simple and intuitive",
    action:
      "Team clearAvenue will use language and functionality that is consistent, easy for every user to understand, with clear accessibility touch points that allows users to always know where they are and how to get where they want to be."
  },
  {
    id: 4,
    description: "Build the service using agile and iterative practices",
    action:
      "The technical team is employing this technique now by providing CMS with an early prototype version of the system. Post-award, this small team of developers will take user feedback, usability tests, and user tracker and version control "
  },
  {
    id: 5,
    description: "Structure budgets and contracts to support delivery",
    action:
      "Our PM will work with the CMS COR and GTL along with clearAvenue Contracts and Finance representatives to oversee the project, ensuring all financial budgets and programmatic requirements are met."
  },
  {
    id: 6,
    description: "Assign one leader and hold that person accountable",
    action:
      "Our technical team lead, Mr. Gabe Helicke, will report to our PM and be held accountable for ensuring the success of the product development. He will have authority to assign tasks, make technical decisions, and ensure all potential users’ needs are included."
  },
  {
    id: 7,
    description: "Bring in experienced teams",
    action:
      "The clearAvenue team of experiences product managers, developers, and designers have experience working with modern development and operations techniques, designing web applications, and using automated testing frameworks"
  },
  {
    id: 8,
    description: "Choose a modern technology stack",
    action:
      "As indicated in Exhibit 5, clearAvenue performed an analysis of various ETL open source, cloud-based, and commodity solutions. For the purposes of this prototype, we chose to use AWS. We understand that dynamic user needs and technological advances will require our design team to re-evaluate which technology stack is appropriate to ensure continued successful user experiences in the future."
  },
  {
    id: 9,
    description: "Deploy a flexible hosting environment",
    action: "Our design team chose to use AWS as the hosting environment."
  },
  {
    id: 10,
    description: "Automate testing and deployment",
    action:
      "Our team used GitLab CLI alongside of AWS CodeDeploy & CodePipelines to automate testing and deployment"
  },
  {
    id: 11,
    description: "Manage security and privacy through reusable processes",
    action:
      "Cybersecurity is a concern for all federal agencies, and that is why security and privacy are an intricate part of the clearAvenue’s solution for CMS. We take into consideration whether access levels, such as whether a user should be able to access, delete, or remove information from the service. Our design team will partner with the appropriate CMS security and records representatives to understand, and ensure users understand, what data is collected and why, how it is used or shared, how it is stored and secured, how long it is kept, whether and how users are notified about how personal information is collected and used, whether a privacy policy is needed and where it should appear, and how users will be notified in the event of a security breach."
  },
  {
    id: 12,
    description: "Use data to drive decisions",
    action:
      "We will monitor real-time system-level resource utilization and system performance.and use this information to create appropriate automated alerts. This information, along with real-time  of concurrent users help the design team determine if the service is meeting users’ needs."
  },
  {
    id: 13,
    description: "Default to open",
    action:
      "The prototype solution is based on open source technology and is designed to be monitored real time to collect feedback for bugs and other issues."
  }
];
