import React from "react";
import PropTypes from "prop-types";
import { Card, CardBody, CardHeader, CardText, CardTitle } from "mdbreact";
import { Row } from "reactstrap";

export default class Helper extends React.Component {
  showHelper;

  constructor(props) {
    super(props);
    this.state = {
      showBody: true
    };
    // this.showHelper= process.env.REACT_APP_SHOW_HELPER;
    this.showHelper = true;
  }

  render() {
    const { header, title, children } = this.props;
    if (this.showHelper) {
      return (
        <Row style={{ width: "100%" }}>
          <Card
            style={{ marginTop: "1rem", width: "100%", fontWeight: "bold" }}
            className="text-center"
          >
            {header && (
              <CardHeader
                color="success-color"
                style={{ backgroundColor: "#008536 !important" }}
              >
                {header}
              </CardHeader>
            )}
            {this.state.showBody && (
              <CardBody className={"text-left"}>
                {title && <CardTitle>{title}</CardTitle>}
                <CardText>{children}</CardText>
              </CardBody>
            )}
          </Card>
        </Row>
      );
    }
  }
}

Helper.prototypes = {
  header: PropTypes.string
};
