import React from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { Container } from "reactstrap";
import { faReact, faAngular, faVuejs } from "@fortawesome/free-brands-svg-icons/index";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome/index.es";
import { ComparisonTableLegend } from "./ComparisonTableLegend";
import { formatter } from "./MeatballFormatter";

export const ClientComparisonTable = props => {
  return (
    <Container style={{ marginBottom: "5rem", marginTop: "1rem" }}>
      <h3>Front End</h3>
      <BootstrapTable
        data={data}
        version={"4"}
        condensed={true}
        responsive={true}
        hover={true}
        striped={true}
        trClassName={"longRow"}
      >
        <TableHeaderColumn
          dataAlign={"center"}
          headerAlign={"center"}
          aria-label={"critiera"}
          isKey
          width={"100"}
          dataField={"criteria"}
        />
        <TableHeaderColumn
          headerAlign={"center"}
          dataField={"react"}
          dataAlign={"center"}
          aria-label={"react"}
          width={"100"}
          dataFormat={formatter}
        >
          <FontAwesomeIcon icon={faReact} size={"3x"} className="cyan-text" />
          <br />
          React <small>by Facebook</small>
        </TableHeaderColumn>
        <TableHeaderColumn
          headerAlign={"center"}
          dataField={"angular"}
          dataAlign={"center"}
          aria-label={"angular"}
          width={"100"}
          dataFormat={formatter}
        >
          <FontAwesomeIcon icon={faAngular} size={"3x"} className="red-text" />
          <br />
          Angular (2+) <small>by Google</small>
        </TableHeaderColumn>
        <TableHeaderColumn
          headerAlign={"center"}
          dataField={"vue"}
          dataAlign={"center"}
          aria-label={"vue"}
          width={"100"}
          dataFormat={formatter}
        >
          <FontAwesomeIcon icon={faVuejs} size={"3x"} className="green-text" />
          <br />
          Vue.js
        </TableHeaderColumn>
      </BootstrapTable>
      <ComparisonTableLegend />
    </Container>
  );
};

const data = [
  {
    criteria: "Open Source",
    react: true,
    angular: true,
    vue: true
  },
  {
    criteria: "Low Learning Curve",
    react: true,
    angular: false,
    vue: true
  },
  {
    criteria: "Virtual DOM",
    react: true,
    angular: false,
    vue: true
  },
  {
    criteria: "Large Community",
    react: true,
    angular: "medium",
    vue: "medium"
  },
  {
    criteria: "Component Focused",
    react: true,
    angular: false,
    vue: false
  },
  {
    criteria: "Scalability",
    react: true,
    angular: true,
    vue: "medium"
  },
  {
    criteria: "Unopinionated",
    react: true,
    angular: false,
    vue: false
  }
];
