import React from "react";
import OktaAuth from "@okta/okta-auth-js/lib/index";
import { withRouter } from "react-router";

const oktaAuth = new OktaAuth({
  url: process.env.REACT_APP_OKTA_BASE_URL,
  clientId: process.env.REACT_APP_OKTA_CLIENT_ID,
  redirectUri: process.env.REACT_APP_OKTA_REDIRECT_URI
});

class ImplicitCallback extends React.PureComponent {
  async componentWillMount() {
    const { history } = this.props;
    let idToken = oktaAuth.tokenManager.get("idToken");
    if (idToken) {
      this.props.setJwt(idToken);
      history.push("/");
    } else if (window.location.hash) {
      let tokens = await oktaAuth.token.parseFromUrl();
      let idToken = tokens[0];
      oktaAuth.tokenManager.add("idToken", idToken);
      this.props.setJwt(idToken);
      history.push("/");
    } else {
      history.push("/login");
    }
  }

  render() {
    return <div />;
  }
}
export default withRouter(ImplicitCallback);
