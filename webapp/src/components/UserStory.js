import React from "react";
import { Card, CardBody, CardHeader } from "mdbreact";

export const UserStory = props => {
  const {
    story: { title, description, acceptanceCriteria, note }
  } = props;
  return (
    <Card style={{ marginBottom: "3rem" }}>
      <CardHeader className={"header"}>
        <strong>{title}</strong>
      </CardHeader>
      <CardBody style={{ height: "350px", overflowY: "auto" }}>
        <strong>Description:</strong>
        <br /> {description}
        <br />
        <br />
        <strong>Acceptance Criteria: </strong>
        <br />
        <ol>
          {acceptanceCriteria.map(criteria => {
            return <li key={"criteria_" + criteria}>{criteria}</li>;
          })}
        </ol>
        {note && <Note note={note} />}
      </CardBody>
    </Card>
  );
};

const Note = props => {
  const { note } = props;
  return (
    <div>
      <br />
      <hr />
      <strong>Note:</strong> {note}
    </div>
  );
};
