import React from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { Container } from "reactstrap";
import { ComparisonTableLegend } from "./ComparisonTableLegend";
import { formatter } from "./MeatballFormatter";

export const RestApiComparisonTable = () => (
  <Container style={{ marginBottom: "5rem", marginTop: "1rem" }}>
    <h3>Back End</h3>
    <BootstrapTable
      data={data}
      version={"4"}
      condensed={true}
      responsive={true}
      hover={true}
      striped={true}
      trClassName={"longRow"}
    >
      <TableHeaderColumn
        dataAlign={"center"}
        headerAlign={"center"}
        aria-label={"critiera"}
        isKey
        dataField={"criteria"}
        width={"100"}
      />
      <TableHeaderColumn
        headerAlign={"center"}
        dataField={"express"}
        dataAlign={"center"}
        aria-label={"Express"}
        dataFormat={formatter}
        width={"100"}
      >
        Express
      </TableHeaderColumn>
      <TableHeaderColumn
        headerAlign={"center"}
        dataField={"hapi"}
        dataAlign={"center"}
        aria-label={"hapi"}
        dataFormat={formatter}
        width={"100"}
      >
        Hapi
      </TableHeaderColumn>
      <TableHeaderColumn
        headerAlign={"center"}
        dataField={"koa"}
        dataAlign={"center"}
        aria-label={"koa"}
        dataFormat={formatter}
        width={"100"}
      >
        Koa
      </TableHeaderColumn>
    </BootstrapTable>
    <ComparisonTableLegend />
  </Container>
);

const data = [
  {
    criteria: "Open Source",
    express: true,
    hapi: true,
    koa: true
  },
  {
    criteria: "Low Learning Curve",
    express: true,
    hapi: true,
    koa: true
  },
  {
    criteria: "Large Community",
    express: true,
    hapi: "medium",
    koa: "medium"
  },
  {
    criteria: "Scalability",
    express: true,
    hapi: true,
    koa: true
  },
  {
    criteria: "Unopinionated",
    express: true,
    hapi: false,
    koa: true
  }
];
