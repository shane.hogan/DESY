import React from "react";
import { Row } from "reactstrap";
import C3Chart from "react-c3js";

const statusTotalsTemplate = columns => ({
  chart: {
    data: {
      columns: columns,
      type: "pie"
    }
  }
});

const ChartStatusTotals = props => {
  const { data } = props;
  return (
    <Row style={{ width: "100%" }}>
      {data && <C3Chart {...statusTotalsTemplate(data.data).chart} />}
    </Row>
  );
};

export default ChartStatusTotals;
