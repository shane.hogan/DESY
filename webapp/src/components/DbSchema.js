import React from "react";
import { Card, CardHeader } from "reactstrap";
import { ListGroup, ListGroupItem, Badge } from "mdbreact";
import * as _ from "lodash";

const DbSchema = props => {
  const { header, columns } = props;
  return (
    <Card>
      <CardHeader>
        <strong>{header}</strong>
      </CardHeader>
      <ListGroup style={{ height: "400px", overflowY: "auto" }}>
        {_.map(columns, column => {
          return (
            <ListGroupItem
              key={column.Name}
              className={"d-flex justify-content-between align-items-center"}
            >
              {column.Name}
              <Badge color={"primary"} pill>
                {column.Type}
              </Badge>
            </ListGroupItem>
          );
        })}
      </ListGroup>
    </Card>
  );
};

export default DbSchema;
