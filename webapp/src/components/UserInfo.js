import React from "react";
import { Col, Navbar } from "reactstrap";
import { Badge } from "mdbreact";

const UserInfo = props => {
  const { authenticated, userProfile, roles } = props;
  if (!authenticated) return "";
  return (
    <Navbar className={"text-center"} style={{ padding: "0px" }}>
      <Col xs={12} className={"text-center"}>
        <h5 className={"h5-responsive"}>
          Welcome, <strong>{formatName(userProfile)}</strong>.{" "}
          <small>
            Current Access Roles:{" "}
            {roles &&
              roles.map(role => {
                return (
                  <Badge color={"green"} style={{ margin: "10px" }} key={role.id}>
                    {role.profile.name}
                  </Badge>
                );
              })}
          </small>
        </h5>
      </Col>
    </Navbar>
  );
};

const formatName = user => {
  if (user.displayName) return user.dispalyName;
  return `${user.firstName || ""} ${user.lastName || ""}`;
};

export default UserInfo;
