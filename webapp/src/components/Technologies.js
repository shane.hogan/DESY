import React from "react";
import { Col, Container, Row } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faReact, faNode, faAws } from "@fortawesome/free-brands-svg-icons";
import {
  faDatabase,
  faServer,
  faExchangeAlt,
  faBalanceScale
} from "@fortawesome/free-solid-svg-icons";

const Technologies = () => {
  return (
    <Container>
      <Row>
        <Col sm="12">
          <Row>
            <section className="text-center my-3">
              <h2 className="h1-responsive font-weight-bold my-5">M.E.R.N Stack</h2>
              <Row>
                <Col xs={12} md={3}>
                  <FontAwesomeIcon icon={faDatabase} size={"6x"} className="green-text" />
                  <h5 className="font-weight-bold my-4">MongoDb</h5>
                  <p className="grey-darken-1 mb-md-0 mb-5">
                    MongoDB is a document database with the scalability and flexibility
                    that you want with the querying and indexing that you need
                  </p>
                </Col>
                <Col xs={12} md={3}>
                  <FontAwesomeIcon icon={faServer} size={"6x"} />
                  <h5 className="font-weight-bold my-4">Express</h5>
                  <p className="grey-darken-1 mb-md-0 mb-5">
                    Fast, unopinionated, minimalist web framework for Node.js
                  </p>
                </Col>
                <Col xs={12} md={3}>
                  <FontAwesomeIcon icon={faReact} size={"6x"} className="cyan-text" />
                  <h5 className="font-weight-bold my-4">React</h5>
                  <p className="grey-darken-1 mb-md-0 mb-5">
                    React is a declarative, efficient, and flexible JavaScript library for
                    building user interfaces.
                  </p>
                </Col>
                <Col xs={12} md={3}>
                  <FontAwesomeIcon icon={faNode} size={"6x"} />
                  <h5 className="font-weight-bold my-4">Node.js</h5>
                  <p className="grey-darken-1 mb-md-0 mb-5">
                    Node.js is an open-source, cross-platform JavaScript run-time
                    environment that executes JavaScript code server-side.
                  </p>
                </Col>
              </Row>
            </section>
          </Row>
          <Row>
            <section className="text-center my-5">
              <h2 className="h1-responsive font-weight-bold my-5">Amazon Web Services</h2>
              <Row className={"justify-content-md-center"}>
                <Col xs={12} md={3}>
                  <FontAwesomeIcon icon={faAws} size={"6x"} className="orange-text" />
                  <h5 className="font-weight-bold my-4">AWS</h5>
                  <p className="grey-darken-1 mb-md-0 mb-5">
                    Amazon Web Services (AWS) is a secure cloud services platform,
                    offering compute power, database storage, content delivery and other
                    functionality to help businesses scale and grow.
                  </p>
                </Col>
                <Col xs={12} md={3}>
                  <FontAwesomeIcon
                    icon={faExchangeAlt}
                    size={"6x"}
                    className="orange-text"
                  />
                  <h5 className="font-weight-bold my-4">Glue</h5>
                  <p className="grey-darken-1 mb-md-0 mb-5">
                    AWS Glue is a fully managed extract, transform, and load (ETL) service
                    that makes it easy for customers to prepare and load their data for
                    analytics.
                  </p>
                </Col>
                <Col xs={12} md={3}>
                  <FontAwesomeIcon
                    icon={faBalanceScale}
                    size={"6x"}
                    className="orange-text"
                  />
                  <h5 className="font-weight-bold my-4">Load Balance</h5>
                  <p className="grey-darken-1 mb-md-0 mb-5">
                    Elastic Load Balancing automatically distributes incoming application
                    traffic across multiple targets, such as Amazon EC2 instances,
                    containers, and IP addresses.
                  </p>
                </Col>
              </Row>
            </section>
          </Row>
        </Col>
      </Row>
    </Container>
  );
};

export default Technologies;
