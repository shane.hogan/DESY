import React from "react";
import {
  faCheck,
  faDotCircle,
  faTimesCircle,
  faMinus
} from "@fortawesome/free-solid-svg-icons/index";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome/index.es";

export const formatter = (cell, row) => {
  if (typeof cell === "boolean") {
    return cell ? (
      <FontAwesomeIcon icon={faCheck} size={"2x"} className="green-text" />
    ) : (
      <FontAwesomeIcon icon={faTimesCircle} size={"2x"} className="red-text" />
    );
  } else if (typeof cell === "string" && cell === "medium") {
    return <FontAwesomeIcon icon={faDotCircle} size={"2x"} className="yellow-text" />;
  } else return <FontAwesomeIcon icon={faMinus} size={"2x"} />;
};
