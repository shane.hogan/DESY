import React from "react";
import { Container } from "reactstrap";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";

const Backlog = () => (
  <Container style={{ marginBottom: "5rem" }}>
    <section className="my-3">
      <BootstrapTable
        data={backlog}
        version={"4"}
        responsive={true}
        hover={true}
        striped={true}
        trClassName={"longRow"}
      >
        <TableHeaderColumn
          dataAlign={"center"}
          headerAlign={"center"}
          aria-label={"id"}
          width={"100"}
          isKey
          dataField={"id"}
          dataSort={true}
        >
          Id
        </TableHeaderColumn>
        <TableHeaderColumn
          headerAlign={"center"}
          dataField={"description"}
          aria-label={"description"}
        >
          Description
        </TableHeaderColumn>
        <TableHeaderColumn
          headerAlign={"center"}
          dataField={"comments"}
          aria-label={"comments"}
          width={"30%"}
          dataAlign={"center"}
          dataSort={true}
        >
          Comments
        </TableHeaderColumn>
      </BootstrapTable>
    </section>
  </Container>
);

export default Backlog;

const backlog = [
  {
    id: 1,
    description:
      "As a ETLaaS user, I need to log in to the system so that I can access the appropriate content.",
    comments: "Included in the prototype"
  },
  {
    id: 2,
    description:
      "As the ETLaaS system, I need to access the Enterprise Privacy Policy (EPPE) system to retrieve user Data Use Agreement (DUA) ",
    comments: "Future"
  },
  {
    id: 3,
    description:
      "As a ETLaaS user, I need to submit a request for an existing extract to be run.",
    comments: "Included in the prototype"
  },
  {
    id: 4,
    description:
      "As a ETLaaS user, I need to create a new extract from an existing data source.",
    comments: "Future"
  },
  {
    id: 5,
    description:
      "As a ETLaaS user, I need to create a new extract from a new data source. ",
    comments: "Future"
  },
  {
    id: 6,
    description:
      "As a ETLaaS user, I need to be able to check the status of my requested extract runs.",
    comments: "Included in the prototype"
  },
  {
    id: 7,
    description:
      "As a ETLaaS user, I need to see the data elements available in the supported data sources.",
    comments: "Included in the prototype"
  },
  {
    id: 8,
    description: "As the ETLaaS system, I need to initiate and run an extract request.",
    comments: "Included in the prototype"
  },
  {
    id: 9,
    description:
      "As the ETLaaS system, I need to deliver a data file, as specified by the user.",
    comments: "Included in the prototype"
  },
  {
    id: 10,
    description:
      "As the ETLaaS system, I need to secure and encrypt delivered files that result from an extract.",
    comments: "Future"
  },
  {
    id: 11,
    description:
      "As the ETLaaS system, I need to deliver extract results to a database environment.",
    comments: "Future"
  },
  {
    id: 12,
    description:
      "As the ETLaaS system, I need to integrate with the Recovery Management and Accounting System (ReMAS) to receive automated requests for claims data.",
    comments: "Future"
  },
  {
    id: 13,
    description:
      "As the ETLaaS system, I need to integrate with the National Claims History (NCH) data repository to provide the user with extract capabilities.",
    comments: "Future"
  },
  {
    id: 14,
    description:
      "As the ETLaaS system, I need to integrate with the National Medicare Utilization Database (NMUD) data repository to provide the user with extract capabilities.",
    comments: "Future"
  },
  {
    id: 15,
    description:
      "As the ETLaaS system, I need to integrate with the Medicare Provider Analysis and Review (MEDPAR) data repository to provide the user with extract capabilities.",
    comments: "Future"
  },
  {
    id: 16,
    description:
      "As the ETLaaS system, I need to integrate with the Standard Analytical Files (SAF) data repository to provide the user with extract capabilities",
    comments: "Future"
  },
  {
    id: 17,
    description:
      "As the ETLaaS system, I need to integrate with the Denominator (DENOM) data repository to provide the user with extract capabilities.",
    comments: "Future"
  },
  {
    id: 18,
    description:
      "As the ETLaaS system, I need to integrate with the Integrated Data Repository (IDR) data repository to provide the user with extract capabilities.",
    comments: "Future"
  },
  {
    id: 19,
    description:
      "As the ETLaaS system, I need to integrate with the Enrollment Data Base (EDB) data repository to provide the user with extract capabilities.",
    comments: "Future"
  }
];
