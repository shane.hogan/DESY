import React from "react";
import {
  Col,
  Container,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane
} from "reactstrap";
import ChartExecAverages from "./ChartExecAverages";
import ChartDailyTotals from "./ChartDailyTotals";
import ChartStatusTotals from "./ChartStatusTotals";

const tabs = [
  "ETL Execution Time Averages",
  "Daily ETL Job Totals",
  "ETL Job Status Totals",
  "ETL Data Source Totals"
];

export default class Charts extends React.Component {
  constructor(props) {
    super(props);
    this.state = { activeTab: tabs[0] };
  }

  componentWillMount() {
    this.props.getAllCharts();
  }

  toggle(chartName) {
    this.setState({ activeTab: chartName });
  }

  render() {
    return (
      <Container fluid>
        <Nav tabs>
          {tabs.map(tab => {
            return (
              <NavItem key={`navItem_${tab}`}>
                <NavLink
                  className={this.state.activeTab === tab ? "active" : ""}
                  onClick={this.toggle.bind(this, tab)}
                >
                  {tab}
                </NavLink>
              </NavItem>
            );
          })}
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          {tabs.map(tab => {
            return this.renderTabContent(tab);
          })}
        </TabContent>
      </Container>
    );
  }

  renderTabContent(chartName) {
    const { execAverages, dailyTotals, statusTotals, dataSourceTotals } = this.props;
    let Comp = "";
    switch (chartName) {
      case "ETL Execution Time Averages":
        Comp = <ChartExecAverages data={execAverages} key={"execAvgs"} />;
        break;
      case "Daily ETL Job Totals":
        Comp = <ChartDailyTotals data={dailyTotals} key={"dailyTotal"} />;
        break;
      case "ETL Job Status Totals":
        Comp = <ChartStatusTotals data={statusTotals} key={"statusTotal"} />;
        break;
      case "ETL Data Source Totals":
        Comp = <ChartStatusTotals data={dataSourceTotals} key={"sourceTotal"} />;
        break;
      default:
        return "";
    }

    return (
      <TabPane tabId={chartName} key={`tabPane_${chartName}`}>
        <Row style={{ width: "100%" }}>
          <Col xs={12} className={"text-center"}>
            <h3 className={"text-center"}>{chartName}</h3>
          </Col>
        </Row>
        <Row style={{ width: "100%" }}>{Comp}</Row>
      </TabPane>
    );
  }
}
