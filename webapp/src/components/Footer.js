import React from "react";
import { Footer as MFooter } from "mdbreact";
import { Col, Container, Row } from "reactstrap";
import image from "../media/cs-logo.svg";

const Footer = () => {
  return (
    <MFooter color="blue" className="footer">
      <Container className="text-left">
        <Row>
          <Col sm={12} md={6}>
            <h5 className="title">Disclaimer</h5>
            <p>
              This demo is a proof of concept that we (clearAvenue, LLC) have built to
              demonstrate our Application. We have not included production-level security,
              nor any production-level data. If you would like to see more, we are more
              than happy to demonstrate the features in a formal walkthrough. To talk to a
              member of our team about this, please contact us at
              http://www.clearavenue.com/contact.html
            </p>
          </Col>
          <Col
            sm={12}
            md={6}
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center"
            }}
          >
            <img src={image} alt={"clearAvenue logo"} />
          </Col>
        </Row>
        <Row>
          <p style={{ margin: "0 auto" }}>Copyright © 2018, clearAvenue LLC.</p>
        </Row>
      </Container>
    </MFooter>
  );
};

export default Footer;
