import React from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import Properties from "../properties";
import * as _ from "lodash";

const formatDataSource = cell => {
  let catalog = _.find(Properties.availableCatalogs, { value: cell }) || {};
  return catalog.label;
};

const statusTypes = {
  STARTING: "STARTING",
  RUNNING: "RUNNING",
  STOPPING: "STOPPING",
  STOPPED: "STOPPED",
  SUCCEEDED: "SUCCEEDED",
  FAILED: "FAILED",
  TIMEOUT: "TIMEOUT"
};

export class RequestStatusTable extends React.Component {
  /**
   * Fix for react-bootstrap-table accessibility
   * Scan for each filter input and add aria-label for SR
   */
  componentDidMount() {
    let elements = document.getElementsByClassName("filter");
    _.forEach(elements, e => {
      e.setAttribute("aria-label", e.placeholder);
    });
  }

  render() {
    const { data } = this.props;

    return (
      <BootstrapTable data={data} version={"4"} pagination>
        <TableHeaderColumn isKey dataField={"_id"} hidden />
        <TableHeaderColumn
          hidden
          dataField={"jobId"}
          aria-label={"job id"}
          columnTitle={true}
          columnClassName={"align-middle center"}
          tdStyle={{ whiteSpace: "normal", wordWrap: "break-word" }}
          filter={{ type: "TextFilter" }}
          dataSort={true}
        >
          Job ID
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField={"DUANNumber"}
          aria-label={"dua number"}
          columnClassName={"align-middle center"}
          columnTitle={true}
          filter={{ type: "TextFilter" }}
          dataSort={true}
        >
          Data Use Agreement (DUA) Number
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField={"dataSource"}
          aria-label={"data source"}
          columnClassName={"align-middle center"}
          columnTitle={true}
          filter={{ type: "TextFilter" }}
          dataSort={true}
          dataFormat={formatDataSource}
        >
          Data Source
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField={"fileName"}
          aria-label={"file name"}
          columnClassName={"align-middle center"}
          columnTitle={true}
          filter={{ type: "TextFilter" }}
          dataSort={true}
        >
          File Name
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField={"format"}
          aria-label={"format"}
          columnClassName={"align-middle center"}
          columnTitle={true}
          filter={{ type: "TextFilter" }}
          dataSort={true}
        >
          Format
        </TableHeaderColumn>
        <TableHeaderColumn
          dataField={"status"}
          columnClassName={"align-middle center"}
          columnTitle={true}
          dataSort={true}
          filter={{ type: "SelectFilter", options: statusTypes }}
          aria-label={"status"}
        >
          Current Status
        </TableHeaderColumn>
      </BootstrapTable>
    );
  }
}
