import React from "react";
import { Col, Row } from "reactstrap";
import C3Chart from "react-c3js";
import _ from "lodash";

const gaugeTemplate = (displayText, data) => ({
  chart: {
    data: {
      columns: [[displayText || "Average Execution Time", data]],
      type: "gauge"
    },
    gauge: {
      min: 0,
      max: data < 60 ? 60 : Math.floor(data + 50),
      units: " sec.",
      label: {
        format: function(value, ratio) {
          return _.round(value, 2);
        }
      },
      size: {
        height: 250
      }
    }
  }
});

class ChartExecAverages extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      rest: ""
    };
  }

  componentWillUpdate(nextProps) {
    if (this.props.data !== nextProps.data && this.state.rest === "") {
      this.setRest();
    }
  }

  componentWillMount() {
    if (this.props.data) {
      this.setRest();
    }
  }

  render() {
    const { data } = this.props;
    let allAvg;
    if (data) {
      allAvg = data.find(d => d.dataSource === "All");
    }
    return (
      <Row style={{ width: "100%" }}>
        <Row style={{ width: "100%" }}>
          <Col xs={12} className={"text-center"}>
            <h3 className={"text-center"}>Average Execution Times</h3>
          </Col>
        </Row>
        {data &&
          allAvg && (
            <Row style={{ width: "100%" }}>
              <C3Chart {...gaugeTemplate("Total Average", allAvg.average).chart} />
            </Row>
          )}
        <Row style={{ width: "100%" }}>{this.state.rest || ""}</Row>
      </Row>
    );
  }

  setRest() {
    const { data } = this.props;
    if (!data) return "";
    let charts = data.map(d => {
      if (d.dataSource === "All") return "";
      const data = gaugeTemplate(`${d.dataSource.replace("Job", "")} Average`, d.average)
        .chart;
      return (
        <Col key={"chart" + d.dataSource} xs={12} md={6} lg={3}>
          <C3Chart {...data} />
        </Col>
      );
    });
    this.setState({ rest: charts });
  }
}

export default ChartExecAverages;
