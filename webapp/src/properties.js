const properties = {
  title: "ETLaaS Prototype",
  menuItems: [
    {
      label: "About",
      showAuthenticated: true,
      showUnauthenticated: true,
      link: "/about"
    },
    {
      label: "ETL Catalogs",
      link: "/catalogs",
      showAuthenticated: true,
      showUnauthenticated: false
    },
    {
      label: "New ETL Request",
      link: "/createRequest",
      showAuthenticated: true,
      showUnauthenticated: false
    },
    {
      label: "ETL Requests Status",
      link: "/requestStatus",
      showAuthenticated: true,
      showUnauthenticated: false
    },
    {
      label: "ETL Reports",
      link: "/reports",
      showAuthenticated: true,
      showUnauthenticated: false
    }
  ],
  availableCatalogs: [
    {
      label: "Enrollment - Provider",
      value: "JobEnrollmentProvider"
    },
    {
      label: "Enrollment - Provider Payments",
      value: "JobEnrollmentProviderPayments"
    },
    {
      label: "Enrollment - In Patients",
      value: "JobEnrollmentInPatient"
    },
    {
      label: "Enrollment - Out Patients",
      value: "JobEnrollmentOutPatient"
    }
  ],
  availableFormats: [
    {
      label: "JSON",
      value: "JSON"
    },
    {
      label: "Parquet",
      value: "Parquet"
    }
  ]
};

export default properties;
