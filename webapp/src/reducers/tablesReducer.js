import * as TableActions from "../actions/tableActions";

const tableReducer = (
  state = {
    isLoading: false,
    tables: null
  },
  action = null
) => {
  switch (action.type) {
    case TableActions.RETRIEVE_TABLES:
      return {
        ...state,
        isLoading: true,
        error: null
      };
    case TableActions.RETRIEVE_TABLES_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };
    case TableActions.RETRIEVE_TABLES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
        tables: action.payload
      };
    default:
      return state;
  }
};

export default tableReducer;
