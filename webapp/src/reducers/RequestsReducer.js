import * as RequestActions from "../actions/RequestActions";
import * as _ from "lodash";
const requestsReducer = (
  state = {
    isLoading: false,
    error: null,
    requests: null
  },
  action = null
) => {
  switch (action.type) {
    case RequestActions.RETRIEVE_PAST_REQUEST:
    case RequestActions.RETRIEVE_ALL_REQUESTS:
    case RequestActions.RETRIEVE_OPEN_REQUEST:
    case RequestActions.CREATE_REQUEST:
      return {
        ...state,
        isLoading: true,
        addedRequest: null,
        requests: null,
        error: null
      };
    case RequestActions.RETRIEVE_OPEN_REQUEST_SUCCESS:
    case RequestActions.RETRIEVE_ALL_REQUEST_SUCCESS:
    case RequestActions.RETRIEVE_PAST_REQUEST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
        requests: action.payload
      };
    case RequestActions.CREATE_REQUEST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
        addedRequest: action.payload
      };
    case RequestActions.RETRIEVE_OPEN_REQUEST_FAILURE:
    case RequestActions.RETRIEVE_ALL_REQUESTS_FAILURE:
    case RequestActions.RETRIEVE_PAST_REQUEST_FAILURE:
    case RequestActions.CREATE_REQUEST_FAILURE:
      return {
        ...state,
        isLoading: false,
        addedRequest: null,
        requests: null,
        error: action.payload
      };
    case RequestActions.CREATE_REQUEST_CLEAR:
      return {
        ...state,
        isLoading: false,
        error: null,
        addedRequest: null
      };
    case RequestActions.DETAILED_REQUEST:
      return {
        ...state,
        details: {
          isLoading: true,
          error: null,
          details: null
        }
      };
    case RequestActions.DETAILED_REQUEST_FAILURE:
      return {
        ...state,
        details: {
          isLoading: false,
          error: action.payload,
          details: null
        }
      };
    case RequestActions.DETAILED_REQUEST_SUCCESS:
      return {
        ...state,
        details: {
          isLoading: false,
          error: null,
          details: action.payload
        }
      };
    case RequestActions.UPDATE_REQUEST_WAIT:
    case RequestActions.UPDATE_ALL_REQUEST:
    case RequestActions.UPDATE_REQUEST:
      return {
        ...state,
        updating: true,
        updateErrors: null
      };
    case RequestActions.UPDATE_REQUEST_WAIT_FAILURE:
    case RequestActions.UPDATE_ALL_REQUEST_FAILURE:
    case RequestActions.UPDATE_REQUEST_FAILURE:
      return {
        ...state,
        updating: false,
        updateErrors: action.payload
      };
    case RequestActions.UPDATE_REQUEST_SUCCESS:
      return {
        ...state,
        updating: false,
        updateErrors: null,
        requests: combineRequests(state.requests || [], action.payload)
      };
    case RequestActions.UPDATE_REQUEST_WAIT_SUCCESS:
    case RequestActions.UPDATE_ALL_REQUEST_SUCCESS:
      return {
        ...state,
        updating: false,
        updateErrors: null,
        requests: action.payload
      };
    default:
      return state;
  }
};

const combineRequests = (requests, updatedRequest) => {
  let match = _.find(requests, { id: updatedRequest.id });
  if (match) {
    let index = _.indexOf(requests, _.find(requests, { id: updatedRequest.id }));
    requests.splice(index, 1, updatedRequest);
  } else {
    requests.push(updatedRequest);
  }
  return requests;
};

export default requestsReducer;
