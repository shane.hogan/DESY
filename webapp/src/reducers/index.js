import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import authReducer from "./authReducer";
import { reducer as formReducer } from "redux-form";
import requestReducer from "./RequestsReducer";
import tableReducer from "./tablesReducer";
import chartReducer from "./chartReducer";
import * as AuthActions from "../actions/authActions";

const reducers = combineReducers({
  router: routerReducer,
  auth: authReducer,
  form: formReducer,
  requests: requestReducer,
  tables: tableReducer,
  charts: chartReducer
});

const appReducer = (state, action) => {
  if (action.type === AuthActions.LOGOUT_SUCCESS) {
    state = undefined;
  }
  return reducers(state, action);
};

export default appReducer;
