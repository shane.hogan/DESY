import * as AuthActions from "../actions/authActions";
import * as _ from "lodash";

const authReducer = (
  state = {
    authenticated: false,
    user: null,
    isLoading: false,
    sessionToken: null
  },
  action = null
) => {
  switch (action.type) {
    case AuthActions.LOGOUT:
    case AuthActions.LOGIN:
      return {
        ...state,
        isLoading: true,
        error: null
      };
    case AuthActions.USER_ROLE:
      return {
        ...state,
        roles: filterRoles(action.payload)
      };
    case AuthActions.LOGIN_SUCCESS:
      return {
        ...state,
        authenticated: true,
        isLoading: false,
        user: action.payload.user || {},
        sessionToken: action.payload.sessionToken,
        rest: action.payload,
        error: null
      };
    case AuthActions.LOGIN_FAILURE:
      return {
        ...state,
        authenticated: false,
        isLoading: false,
        user: null,
        error: action.payload
      };
    case AuthActions.LOGOUT_SUCCESS:
      localStorage.removeItem("ETLaaS-jwt-token");
      return {
        ...state,
        authenticated: false,
        isLoading: false,
        jwt: null,
        sessionToken: null,
        user: null,
        error: null
      };
    case AuthActions.JWT_TOKEN:
      localStorage.setItem("ETLaaS-jwt-token", action.payload);
      return {
        ...state,
        jwt: action.payload.idToken
      };
    case AuthActions.UPDATE_AUTHENTICATED:
      let authenticated = action.payload;
      return {
        ...state,
        authenticated: authenticated,
        user: authenticated ? state.user : null
      };
    default:
      return state;
  }
};

const filterRoles = roles => {
  return _.map(_.reject(roles, ["profile.name", "Everyone"]), role => {
    return {
      id: role.id,
      profile: role.profile
    };
  });
};

export default authReducer;
