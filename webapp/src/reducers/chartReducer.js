import * as ChartActions from "../actions/chartActions";

const chartReducer = (state = {}, action = null) => {
  switch (action.type) {
    case ChartActions.GET_EXEC_AVERAGES_SUCCESS:
      return {
        ...state,
        execAverages: action.payload
      };
    case ChartActions.GET_DAILY_TOTALS_SUCCESS:
      return {
        ...state,
        dailyTotals: action.payload
      };
    case ChartActions.GET_STATUS_TOTALS_SUCCESS:
      return {
        ...state,
        statusTotals: action.payload
      };
    case ChartActions.GET_DATA_SOURCE_TOTALS_SUCCESS:
      return {
        ...state,
        dataSourceTotals: action.payload
      };
    case ChartActions.GET_ALL_CHARTS:
      return {
        isLoading: true
      };
    case ChartActions.GET_ALL_CHARTS_SUCCESS:
      return {
        allCharts: action.payload,
        isLoading: false,
        error: null
      };
    case ChartActions.GET_ALL_CHARTS_FAILURE:
      return {
        isLoading: false,
        error: action.payload
      };
    default:
      return state;
  }
};

export default chartReducer;
