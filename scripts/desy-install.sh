#!/bin/bash
echo "Install React dependencies"
cd /home/ubuntu/DESY/webapp
npm install --production
echo "Build React"
npm run build

cd /home/ubuntu/DESY/api-server
echo "Install express dependencies"
npm install --production
echo "Build Express"
npm run build